import path from "path";
import fs from "fs";
import jwt from "jsonwebtoken";

export default function handler(req, res) {
  const { filename } = req.query;
  const { shop } = req.query;

  if (!shop) {
    return res.status(400).json({ error: "Shop parameter is required" });
  }

  // Verify the request is coming from a valid Shopify store
  try {
    const token = req.headers.authorization?.split(" ")[1];
    if (!token) {
      // For image requests, we'll be more lenient and not require authentication
      // This allows the images to load in img tags
      console.log("No auth token provided for image request");
    } else {
      // If token is provided, verify it
      jwt.verify(token, process.env.SHOPIFY_API_SECRET);
    }

    const filePath = path.join(
      process.cwd(),
      "server",
      "assets",
      "images",
      filename
    );

    const imageBuffer = fs.readFileSync(filePath);
    const ext = path.extname(filename).toLowerCase();
    const contentType =
      {
        ".png": "image/png",
        ".jpg": "image/jpeg",
        ".jpeg": "image/jpeg",
        ".gif": "image/gif",
        ".svg": "image/svg+xml",
      }[ext] || "application/octet-stream";

    // Set CORS headers to allow the Shopify admin to load the images
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET");
    res.setHeader("Access-Control-Allow-Headers", "Authorization");

    // Set caching headers for better performance
    res.setHeader("Cache-Control", "public, max-age=86400"); // Cache for 24 hours
    res.setHeader("Content-Type", contentType);

    res.send(imageBuffer);
  } catch (error) {
    console.error("Error serving image:", error);
    res.status(404).json({ error: "Image not found or unauthorized access" });
  }
}
