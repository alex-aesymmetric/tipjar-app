const registerWebhooks = async (shop, accessToken) => {
  const webhooks = [
    {
      topic: "APP_UNINSTALLED",
      address: `${process.env.HOST}/api/webhooks/app/uninstalled`,
    },
    {
      topic: "APP_INSTALLED",
      address: `${process.env.HOST}/api/webhooks/app/installed`,
    },
  ];

  for (const webhook of webhooks) {
    await registerWebhook(shop, accessToken, webhook.topic, webhook.address);
  }
};
