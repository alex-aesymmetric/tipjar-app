import "url-search-params-polyfill";
import { useState, useEffect } from "react";
import { hotjar } from "react-hotjar";
import { initGA, logPageView } from "../utils/analytics";
import App from "next/app";
// shopify imports
import { AppProvider } from "@shopify/polaris";
import { Provider as AppBridgeProvider } from "@shopify/app-bridge-react";
import "@shopify/polaris/styles.css";
import translations from "@shopify/polaris/locales/en.json";
// import components
import Layout from "../components/Layout";
import AppLayout from "../components/AppLayout";
// import store provider
import { StoreProvider } from "../store/useStore";

// ***********
// component
// ***********

export default function MyApp({ pageProps, Component }) {
  // const { Component, pageProps, shopOrigin, host } = this.props; ? debug
  // load tracking and analytics
  useEffect(() => {
    hotjar.initialize(HJID, HJSV);

    if (!window.GA_INITIALIZED) {
      initGA();
      window.GA_INITIALIZED = true;
    }
    logPageView();
  }, []);

  // New Config app-bridge-react @ '^3.x.x'
  const config = {
    apiKey: API_KEY,
    // The host of the specific shop that's embedding your app.
    // This value is provided by Shopify as a URL query parameter,
    // that's appended to your application URL when your app is,
    // loaded inside the Shopify admin.
    host: pageProps.host,
    forceRedirect: true,
  };

  // testing is host comming from pageProps:
  // console.log("PageProps host? :", pageProps.host); // ? debug

  return (
    <AppBridgeProvider config={config}>
      <AppProvider i18n={translations}>
        <StoreProvider>
          <AppLayout>
            <Layout {...pageProps}>
              <Component {...pageProps} />
            </Layout>
          </AppLayout>
        </StoreProvider>
      </AppProvider>
    </AppBridgeProvider>
  );
}

// get initial props from server, before page/window load
MyApp.getInitialProps = async (appContext) => {
  // const { shop: shopOrigin, host: host } = appContext.ctx.query;
  const shopOrigin = appContext.ctx.query.shop;
  const host = appContext.ctx.query.host;
  // console.log("MyAPP getInitialProps: "); // ? debug
  // console.log("MyAPP ShopOrigin: ", shopOrigin); // ? debug
  // console.log("MyAPP host: ", host); // ? debug
  return { pageProps: { shopOrigin: shopOrigin, host: host } };
};
