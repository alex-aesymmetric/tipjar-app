import { useState, useEffect } from "react";
import { Page, Layout, FormLayout, Card, PageActions } from "@shopify/polaris";
// import utils
import { isEqual } from "lodash";
// import components
import FooterHelpDiv from "../components/FooterHelp";
import Head from "../components/Head";
import Initialize from "../components/SettingsPage/Initialize";
import Loading from "../components/Loading";
import Settings from "../components/SettingsPage/Settings";
import TopBannerInformation from "../components/TopBannerInformation";
// import styles
import "../components/custom.css";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

export default function Index(props) {
  const { state, dispatch } = useStore();

  // declare settings variable for components below
  const settings = state.newSettings || state.shopSettings;

  const primaryAction =
    state.newSettings && !isEqual(state.newSettings, state.shopSettings)
      ? {
          content: "Update settings",
          onAction: () =>
            ACTIONS.updateSettings_shop(state, dispatch, state.newSettings),
          loading: state.metafields_updating,
        }
      : null;

  return (
    <>
      <Head title="Settings" path="/" />
      <Page title="Settings" primaryAction={primaryAction}>
        <Settings settings={settings} />
        <PageActions primaryAction={primaryAction} />
      </Page>
      <FooterHelpDiv />
    </>
  );
}
