import { Page, Layout, Card, Link, TextStyle } from "@shopify/polaris";
// import components
import FooterHelpDiv from "../components/FooterHelp";
import Head from "../components/Head";
// import store / actions
import useStore from "../store/useStore";

export default function MoreApps() {
  const { state, dispatch } = useStore();

  // Get shop from state
  const shop = state?.shopDetails?.shop_domain;

  const otherApps = [
    {
      name: "Donation Pop",
      description:
        "Give your customers the option to add a donation to their order before checkout.",
      logo: `/api/images/donation-pop-app-logo.png?shop=${shop}`,
      link:
        "https://apps.shopify.com/donation-pop?utm_source=tip-quik&utm_campaign=more-apps-page&utm_medium=In+app+banner",
    },
  ];

  const partners = [
    {
      name: "PreProduct",
      description:
        "A smarter way to launch products. Pay-later, pay-now & deposit-based preorders.",
      logo: `/api/images/preproduct-app-logo.png?shop=${shop}`,
      link:
        "https://apps.shopify.com/4970c9de7b8f351239aaf47add291532?utm_source=appfriends&utm_campaign=tip-quik-1&utm_medium=In+app+banner",
    },
    // {
    //   name: "PageFly",
    //   description: "PageFly is a highly-efficient tool to create well-designed pages that drive sales for your online store.",
    //   logo: "/images/pagefly-logo.png",
    //   link: "#",
    // },
    // {
    //   name: "Personizely",
    //   description: "Personizely combines email popups, exit intent, and upsell features to optimize your CR and sales.",
    //   logo: "/images/personizely-logo.png",
    //   link: "#",
    // },
    // {
    //   name: "Adoric",
    //   description: "Adoric is a powerful tool for creating smart & easy product recommendations & popups that boost your AOV. Collect Emails and Upsell Cross Sell.",
    //   logo: "/images/adoric-logo.png",
    //   link: "#",
    // },
  ];

  return (
    <>
      <Head title="More Apps" path="/more-apps" />
      <Page title="More Apps">
        <Layout>
          <Layout.Section>
            <Card sectioned title="Our Other Apps">
              <p>
                Check out our other apps we've made that will help improve your
                store!
              </p>
              {otherApps.map((partner, index) => (
                <div key={index}>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "flex-start",
                      gap: "20px",
                      paddingTop: "30px",
                    }}
                  >
                    <div
                      style={{ flexShrink: 0, width: "60px", height: "60px" }}
                    >
                      <img
                        src={partner.logo}
                        alt={`${partner.name} logo`}
                        style={{
                          width: "100%",
                          height: "100%",
                          objectFit: "contain",
                        }}
                      />
                    </div>
                    <div>
                      <TextStyle variation="strong">{partner.name}</TextStyle>
                      <p style={{ margin: "0.5em 0" }}>{partner.description}</p>
                      <Link url={partner.link} external>
                        Explore
                      </Link>
                    </div>
                  </div>
                </div>
              ))}
            </Card>
            <Card sectioned title="Our App Partners">
              <p>
                Check out these other great Shopify apps that we partner with!
              </p>
              {partners.map((partner, index) => (
                <div key={index}>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "flex-start",
                      gap: "20px",
                      paddingTop: "30px",
                    }}
                  >
                    <div
                      style={{ flexShrink: 0, width: "60px", height: "60px" }}
                    >
                      <img
                        src={partner.logo}
                        alt={`${partner.name} logo`}
                        style={{
                          width: "100%",
                          height: "100%",
                          objectFit: "contain",
                        }}
                      />
                    </div>
                    <div>
                      <TextStyle variation="strong">{partner.name}</TextStyle>
                      <p style={{ margin: "0.5em 0" }}>{partner.description}</p>
                      <Link url={partner.link} external>
                        Explore
                      </Link>
                    </div>
                  </div>
                </div>
              ))}
            </Card>
          </Layout.Section>
          <FooterHelpDiv />
        </Layout>
      </Page>
    </>
  );
}
