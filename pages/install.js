import { useState, useEffect, useCallback } from "react";
import { Page, Layout, Card, TextStyle, Link, Select } from "@shopify/polaris";
// import utils
import API from "../utils/api";
import snippetContent from "../utils/snippetContent";
// import components
import FooterHelpDiv from "../components/FooterHelp";
import Head from "../components/Head";
import Loading from "../components/Loading";
// import styles
import "../components/custom.css";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

export default function Install(props) {
  const { state, dispatch } = useStore();

  const appEmbed =
    process.env.APP_EMBED || "61034a2c-6b86-4802-9c8f-bfb1cb9a3360";
  const [activeTheme, setActiveTheme] = useState("");
  const [loadingTheme, setLoadingTheme] = useState(false);
  const [themes, setThemes] = useState(null);
  const fetchThemes = async () => {
    setLoadingTheme(true);
    const res = await API({
      method: "GET",
      url: `/shop/theme`,
    });
    const options = [];
    let activeId = 0;
    res.data.forEach((item) => {
      if (item.role === "main") {
        item.name = item.name + "(Current theme)";
        activeId = item.id;
      }
      options.push({ label: item.name, value: item.id });
    });

    setActiveTheme(activeId);
    setThemes(options);
    setLoadingTheme(false);
  };
  useEffect(() => {
    fetchThemes();
  }, []);
  const handleSelectChange = useCallback((value) => {
    setActiveTheme(parseInt(value));
  }, []);
  return (
    <>
      <Head title="Install" path="/install" />
      <Page title="Installation Steps">
        <Layout>
          <Layout.Section>
            {/* <Card
              sectioned
              title="Step 1. Create the snippet"
              primaryFooterAction={{
                content: "Install app snippet",
                loading: state.snippet_creating,
                disabled: state.snippet_created,

                onAction: () =>
                  ACTIONS.createSnippet(state, dispatch, snippetContent),
              }}
            >
              <p>
                Click the button below to install the{" "}
                <TextStyle variation="code">tipquik.liquid</TextStyle> snippet
                on your current published theme. This is the app code that
                handles the tip pop-up modal.
              </p>
              {state.snippet_created && (
                <p className="install-des">
                  The app snippet has been created. Click the 'Install app
                  snippet' button again to re-install.
                </p>
              )}
            </Card> */}

            {/* <Card title="Step 2. Add the snippet to your theme" sectioned>
              <p>
                Add the code below to your{" "}
                <TextStyle variation="code">theme.liquid</TextStyle> file on
                your current published theme, directly above the{" "}
                <TextStyle variation="code">{"</body>"}</TextStyle> tag. This
                code connects the app snippet to your theme code.
              </p>
              <br />
              <br />
              <p>
                <TextStyle variation="code">{`{% include 'tipquik' %}`}</TextStyle>
              </p>
              <br />
              <br />
              <p>
                Not sure how to do this?
                <span className="install-email">
                  <Link external url="https://vimeo.com/450159271">
                    Watch the installation walkthrough video
                  </Link>
                </span>
              </p>
            </Card> */}

            <Card
              title="Step 1. Enable the theme app extension"
              sectioned
              primaryFooterAction={{
                content: "Preview in theme",
                url: `http://${state?.shopDetails.shop_domain}/admin/themes/${activeTheme}/editor?context=apps&appEmbed=${appEmbed}/tipquik`,
                target: "_blank",
                external: true,
                loading: loadingTheme,
              }}
            >
              <p>
                Select a theme on your store from the list below that you would
                like to enable Tip Quik on, No coding required, all done
                automatically for you!
              </p>
              <p style={{ marginTop: "20px" }}>
                <Select
                  label="Store themes"
                  options={themes}
                  onChange={handleSelectChange}
                  value={activeTheme}
                />
              </p>
            </Card>
            <Card
              title="Step 2. Create the product"
              sectioned
              primaryFooterAction={{
                content: "Create tip/gratuity product",
                loading: state.product_creating,
                onAction: () => ACTIONS.createProduct(state, dispatch),
              }}
            >
              <p>
                Click the button below to create the Tip/Gratuity product. This
                product is necessary and used to add the tip/gratuity amount to
                the order.
              </p>
              {state.product_created && (
                <p className="install-des">
                  The Tip/Gratuity product has been created. You can click the
                  'Create tip/gratuity product' button again to re-create it if
                  needed.
                </p>
              )}
            </Card>

            {/* // ! currently inactive (INCOMPLETE) */}
            {/* <div className="card-spliter" />
            <Card
              sectioned
              title="Need help with installation?"
              primaryFooterAction={{
                content: "Request Help",
                onAction: () => ACTIONS.requestHelp(state, dispatch),
                disabled: state.installHelpStatus,
              }}
            >
              {state.installHelpStatus && (
                <p>
                  Your help request has been sent. We will reach out to the{" "}
                  <span className="install-email">
                    {state.shopDetails?.store_owner_email}
                  </span>{" "}
                  address soon.
                </p>
              )}
            </Card> */}
            <FooterHelpDiv />
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );
}
