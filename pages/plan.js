import { useState, useEffect } from "react";
import { Page, Layout, Card, Link, Badge } from "@shopify/polaris";
import NextLink from "next/link";
// import utils
import API from "../utils/api";
//import { triggerIntercom } from "../utils/triggerIntercom";
// import components
import FooterHelpDiv from "../components/FooterHelp";
import Head from "../components/Head";
// import styles
import "../components/custom.css";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

export default function Plan(props) {
  const { state, dispatch } = useStore();

  // component states
  const currentPlanString = `${state.subscriptionPlans[state.currentPlan]}`;

  useEffect(() => {
    // only call if browser window exists
    if (typeof window !== "undefined") {
      ACTIONS.checkForSubscriptionUpdate(state, dispatch);
    }
  }, []);

  // function used below to determine card.sections actions based on current plan
  const planActions = (plan) => {
    return state.currentPlan === 0
      ? {
          content: "Subscribe",
          loading: state.subscription_updating,
          onAction: () => ACTIONS.setSubscription(state, dispatch, plan),
          disabled: state.metafields_installed === false,
        }
      : {
          content: "Unsubscribe",
          destructive: state.subscription_updating ? false : true,
          loading: state.subscription_updating,
          onAction: () => ACTIONS.setSubscription(state, dispatch, "Free"),
          disabled: state.metafields_installed === false,
        };
  };

  return (
    <>
      <Head title="Account" path="/plan" />
      <Page
        title="Your Tip Quik Account"
        titleMetadata={<Badge status="info">{currentPlanString}</Badge>}
      >
        <Layout>
          <Layout.Section>
            <Card
              title={
                <>
                  <h2
                    className="Polaris-Heading"
                    style={{ display: "inline-block", paddingRight: "1rem" }}
                  >
                    Plan List
                  </h2>
                  {state.metafields_installed === true ? null : (
                    <i>
                      (You must{" "}
                      <NextLink href="/">initialize the app settings</NextLink>{" "}
                      before you can change your plan)
                    </i>
                  )}
                </>
              }
            >
              <Card.Section>
                <Card.Section title="Free">
                  <p>
                    Collect tips from your customers before checkout. Completely
                    free to use!
                  </p>
                </Card.Section>

                {/* // ! currently inactive */}
                {/* <Card.Section
                  title="Standard ($4.99/month)"
                  actions={[planActions("Standard")]}
                >
                  <p>You have no limitations.</p>
                </Card.Section> */}

                <Card.Section
                  title="Premium ($9.99/month)"
                  actions={[planActions("Premium")]}
                >
                  <p>Includes more ways to customize the tip modal:</p>
                  <ul>
                    <li>
                      Show a custom tip amount option on the tip modal pop-up
                    </li>
                    <li>
                      Hide the 'Powered by Tip Quik' text on the tip modal
                      pop-up
                    </li>
                    <li>
                      Development help to fully customize the tip modal design
                    </li>
                    <li>
                      Development help to customize the tip modal functionality
                    </li>
                    <li>Access to more premium options as they are released</li>
                  </ul>
                </Card.Section>
              </Card.Section>
            </Card>
            <Card
              sectioned
              title="Need help? App not working? Tip modal not appearing? Questions? Comments?"
            >
              <p>
                Don't hesitate to get in touch with us! We will be happy to
                assist you with anything Tip Quik related. Contact us via email
                at
                <span className="install-email">
                  <Link external url="mailto:support@aesymmetric.xyz">
                    support@aesymmetric.xyz
                  </Link>
                </span>
                We’ll get back to you as soon as we can!
              </p>
            </Card>
            <FooterHelpDiv />
          </Layout.Section>
        </Layout>
      </Page>
    </>
  );
}
