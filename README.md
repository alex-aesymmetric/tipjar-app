# Local Development

To use this app in local development mode, perform the below steps. Steps 1 and 2 are only needed for initial setup, unless you change something about them. Perform step 3 anytime there are changes to your front-end code.

## 1. Fetch env vars from production

- In console, run command 'heroku config'
- Keep for below steps

## 2. Update (or create) local .env file

- Copy production env vars (from above) into .env (format as necessary)
- Run command 'shopify connect'
  - Select 'Node.js App'
  - Select 'Aesymmetric'
  - Select 'Tipquik Local'
  - Select 'alex-test-store' (Or whatever store you like. Just make sure the store doesn't have Tipquik in production already.)
- In .env, replace APP_NAME with 'Tipquik Local'

## 3. Serve local environment

- Run command 'npm run build && shopify serve'
- If asked to update host url, click YES

## 4. Install app on store

- Click provided install link in console
- Shows 'To install and start using your app, open this URL in your browser'
- **If install does NOT work, returning "JWT is not valid" in console, you can temporarily comment out all verifyJwt calls in server.js, only while developing locally. For some unknown reason, this happens intermittently.**

## 5. Uninstall app from store

- **Only needed for back-end changes**
- Once changes are made, restart at STEP 3

# Deployment

When deploying to production, make sure of the following before pushing to Heroku:

- Locally, Node.js version is set to 16.x (as in package.json). This can be done by running `nvm use`
- Locally, NPM version is set to 6.x (as in package.json). This can be done by running `nvm use`
- Run `npm install`
- Switch to production config using `shopify app config use` so we can point theme app extension updates to production app (if updating app extension, don't forget to also update "store-customization-app-snippet.liquid" for future store customization requests)
- Deploy and Shopify app config changes to Shopify Partners dashboard using `shopify app deploy`
- Confirm on Heroku that the Node.js and NPM versions are set to 16.x and 6.x respectively.
