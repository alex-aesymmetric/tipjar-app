// import utils / hooks
import API from "../utils/api";

export const actionTypes = {
  GET_SHOP_DETAILS: "GET_SHOP_DETAILS",
  GET_SHOP_SETTINGS: "GET_SHOP_SETTINGS",
  INSTALL_SHOP_SETTINGS: "INSTALL_SHOP_SETTINGS",
  UPDATE_SETTINGS: "UPDATE_SETTINGS",
  UPDATE_SHOP_SETTINGS: "UPDATE_SHOP_SETTINGS",
  SET_CURRENT_PLAN: "SET_CURRENT_PLAN",
  METAFIELDS_UPDATING: "METAFIELDS_UPDATING",
  METAFIELDS_UPDATED: "METAFIELDS_UPDATED",
  METAFIELDS_INSTALLING: "METAFIELDS_INSTALLING",
  METAFIELDS_INSTALLED: "METAFIELDS_INSTALLED",
  SNIPPET_CREATING: "SNIPPET_CREATING",
  SNIPPET_CREATED: "SNIPPET_CREATED",
  PRODUCT_CREATING: "PRODUCT_CREATING",
  PRODUCT_CREATED: "PRODUCT_CREATED",
  SUBSCRIPTION_UPDATING: "SUBSCRIPTION_UPDATING",
  SUBSCRIPTION_UPDATED: "SUBSCRIPTION_UPDATED",
};

// ***********
// action
// ***********

// get shop details and settings from DATABASE
export const getShopData = async (state, dispatch) => {
  await getShopDetails(state, dispatch);
  await getShopSettings(state, dispatch);
};

// ***********
// action
// ***********

// get shop details from DATABASE
export const getShopDetails = async (state, dispatch) => {
  const res = await API({
    method: "GET",
    url: "/shop",
  });
  const shopDetails = res.data;

  dispatch({
    type: actionTypes.GET_SHOP_DETAILS,
    payload: shopDetails,
  });
};

// ***********
// action
// ***********

// get shop settings from DATABASE
export const getShopSettings = async (state, dispatch) => {
  const res = await API({
    method: "GET",
    url: "/shop/settings",
  });
  const shopSettings = res.data;

  dispatch({
    type: actionTypes.GET_SHOP_SETTINGS,
    payload: shopSettings,
  });
};

// ***********
// action
// ***********

// set current plan
export const setCurrentPlan = async (state, dispatch, newPlan) => {
  const subscription_id = state.subscriptionPlans.findIndex(
    (plan) => plan === newPlan
  );

  const res = await API({
    method: "POST",
    url: "plan/optimistic",
    data: { subscription_id },
  });
  const currentPlan = res.data;

  dispatch({
    type: actionTypes.SET_CURRENT_PLAN,
    payload: { subscription_id },
  });
};

// ***********
// action
// ***********

// install shop settings on SHOPIFY
export const installSettings_shop = async (state, dispatch) => {
  dispatch({
    type: actionTypes.METAFIELDS_INSTALLING,
    payload: { metafields_installing: true, metafields_installed: false },
  });

  const res = await API({
    method: "POST",
    url: "/shop/metafields",
  });
  const installedSettings = res.data.values;

  // refetch settings to make sure up to date
  await getShopData(state, dispatch);

  dispatch({
    type: actionTypes.METAFIELDS_INSTALLED,
    payload: { metafields_installing: false, metafields_installed: true },
  });
};

// ***********
// action
// ***********

// update shop settings on UI
export const updateSettings = async (state, dispatch, newSettings) => {
  dispatch({
    type: actionTypes.UPDATE_SETTINGS,
    payload: newSettings,
  });
};

// ***********
// action
// ***********

// update shop settings on SHOPIFY
export const updateSettings_shop = async (state, dispatch, newSettings) => {
  dispatch({
    type: actionTypes.METAFIELDS_UPDATING,
    payload: true,
  });

  const res = await API({
    method: "PUT",
    url: "/shop/metafields",
    data: { metafields_value: newSettings },
  });
  const updatedSettings = res.data.values;

  // refetch settings to make sure up to date
  await getShopSettings(state, dispatch);

  dispatch({
    type: actionTypes.METAFIELDS_UPDATED,
    payload: false,
  });
};

// ***********
// action
// ***********

// create shopify theme snippet
export const createSnippet = async (state, dispatch, snippetContent) => {
  dispatch({
    type: actionTypes.SNIPPET_CREATING,
    payload: { snippet_creating: true, snippet_created: false },
  });

  const res = await API({
    method: "POST",
    url: `/snippet`,
    data: {
      asset: {
        key: "snippets/tipquik.liquid",
        value: snippetContent,
      },
    },
  });
  const snippet_created = res.data;

  dispatch({
    type: actionTypes.SNIPPET_CREATED,
    payload: { snippet_creating: false, snippet_created: true },
  });
};

// ***********
// action
// ***********

// create shopify gratuity / tip item
export const createProduct = async (state, dispatch) => {
  dispatch({ type: "SET_PRODUCT_CREATING", payload: true });

  try {
    const createRes = await API({
      method: "POST",
      url: "/product",
    });

    // Handle the GraphQL response
    if (!createRes.data || !createRes.data.product || !createRes.data.shop) {
      throw new Error("Invalid response structure from API");
    }

    // Update both product creation status and shop details
    dispatch({
      type: actionTypes.GET_SHOP_DETAILS,
      payload: createRes.data.shop,
    });

    dispatch({ type: "SET_PRODUCT_CREATING", payload: false });

    // Show success toast/notification
    if (window.app) {
      window.app.show({
        type: "success",
        content: "Tip/Gratuity product created successfully",
      });
    }
  } catch (error) {
    console.error("Error creating product:", error);
    dispatch({ type: "SET_PRODUCT_CREATING", payload: false });

    // Show error toast/notification
    if (window.app) {
      window.app.show({
        type: "error",
        content: error.response?.data?.error || "Failed to create product",
      });
    }
  }
};

// ***********
// action
// ***********

// function updates/sets the subscription
export const setSubscription = async (state, dispatch, plan) => {
  dispatch({
    type: actionTypes.SUBSCRIPTION_UPDATING,
    payload: { subscription_updating: true },
  });

  // call to subscription API endpoint
  const res = await API({
    method: "POST",
    url: `/plan/${plan.toLowerCase()}`,
    data: {
      shop_owner: state.shopDetails?.store_owner_email,
      shop_domain: state.shopDetails?.shop_domain,
      plan: plan,
    },
  });

  // check if plan free or paid
  // if setting plan to PAID
  if (plan !== "Free") {
    const confirmationUrl = res.data;

    // set plan in localstorage to perform database subscription check after clicking 'approve subscription'
    localStorage.setItem("plan", plan);

    // REDIRECT to subscription confirmation URL (user approves here)
    window.top.location.replace(confirmationUrl);
  }

  // if setting plan to FREE
  else {
    // set current plan to 'Free'
    await setCurrentPlan(state, dispatch, "Free");

    dispatch({
      type: actionTypes.SUBSCRIPTION_UPDATED,
      payload: { subscription_updating: false },
    });
  }
};

// ***********
// action
// ***********

// function checks if subscription has been updated when returning from upgrade confirmation
export const checkForSubscriptionUpdate = async (state, dispatch) => {
  // get plan from return url query param
  const plan_url = await new URLSearchParams(
    typeof window !== "undefined" ? window.location.search : null
  ).get("plan");

  // get plan from local storage
  const plan_local = await localStorage.getItem("plan");

  // check if plan_url and plan_local exist
  if (plan_url && plan_local) {
    // compare plan values
    // if equal, set currentPlan component state
    // else, do nothing
    if (plan_url.toLowerCase() === plan_local.toLowerCase()) {
      await setCurrentPlan(state, dispatch, plan_local);

      dispatch({
        type: actionTypes.SUBSCRIPTION_UPDATED,
        payload: { subscription_updating: false },
      });
    }
  }

  // remove localStorage plan after completion for security
  localStorage.removeItem("plan");
};

// ***********
// action
// ***********

// request help
// ! currently inactive (INCOMPLETE)
// export const requestHelp = async (state, dispatch) => {

//   const res = await API({
//     method: "POST",
//     url: `/help`,
//     data: {
//       store_data: {
//         shop_owner: state.shopDetails?.store_owner_email,
//         shop_domain: state.shopDetails?.shop_domain,
//       },
//     },
//   });
//   const help_requested = res.data;

//   dispatch({
//     type: actionTypes.HELP_REQUESTED,
//     payload: true,
//   });
// };
