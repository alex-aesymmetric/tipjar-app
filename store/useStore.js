// @ts-nocheck
import React, { createContext, useContext, useReducer, useEffect } from "react";
import isEmpty from "lodash/fp/isEmpty";
// import actions / action types
import { actionTypes, getShopData } from "./actions";

// ***********
// reducer
// ***********

const reducer = (state, action) => {
  console.log("*** action:", action.type); // ? debug

  switch (action.type) {
    // *** auth actions
    case actionTypes.GET_SHOP_DETAILS:
      return {
        ...state,
        shopDetails: action.payload,
        currentPlan: action.payload.subscription_plan,
        isFreePlan: action.payload.subscription_plan === 0,
        metafields_installed: action.payload.shopify_metafields_created,
        snippet_created: action.payload.snippet_installation_status,
        product_created: action.payload.product_installation_status,
        help_requested: action.payload.installation_help_status,
      };
    case actionTypes.GET_SHOP_SETTINGS:
      return { ...state, shopSettings: action.payload };
    case actionTypes.UPDATE_SETTINGS:
      return { ...state, newSettings: action.payload };
    case actionTypes.SET_CURRENT_PLAN:
      return {
        ...state,
        currentPlan: action.payload.subscription_id,
        isFreePlan: action.payload.subscription_id === 0,
      };
    case actionTypes.METAFIELDS_UPDATING:
    case actionTypes.METAFIELDS_UPDATED:
      return { ...state, metafields_updating: action.payload };
    case actionTypes.METAFIELDS_INSTALLING:
    case actionTypes.METAFIELDS_INSTALLED:
      return {
        ...state,
        metafields_installing: action.payload.metafields_installing,
        metafields_installed: action.payload.metafields_installed,
      };
    case actionTypes.SNIPPET_CREATING:
    case actionTypes.SNIPPET_CREATED:
      return {
        ...state,
        snippet_creating: action.payload.snippet_creating,
        snippet_created: action.payload.snippet_created,
      };
    case actionTypes.PRODUCT_CREATING:
    case actionTypes.PRODUCT_CREATED:
      return {
        ...state,
        product_creating: action.payload.product_creating,
        product_created: action.payload.product_created,
      };
    case actionTypes.SUBSCRIPTION_UPDATING:
    case actionTypes.SUBSCRIPTION_UPDATED:
      return {
        ...state,
        subscription_updating: action.payload.subscription_updating,
      };
    case "SET_PRODUCT_CREATING":
      return {
        ...state,
        product_creating: action.payload,
      };

    case "SET_PRODUCT_CREATED":
      return {
        ...state,
        product_created: action.payload,
      };
    default:
      return state;
  }
};

// ***********
// initial state
// ***********

const initState =
  typeof window !== "undefined"
    ? {
        shopDetails: null,
        shopSettings: null,
        currentPlan: 0,
        isFreePlan: true,
        subscriptionPlans: ["Free", "Standard", "Premium"],
        savedSettingsLoading: false,
        metafields_installing: false,
        metafields_installed: false,
        metafields_updating: false,
        savedSettings: null,
        newSettings: null,
        emptyError: false,
        snippet_creating: false,
        snippet_created: false,
        product_creating: false,
        product_created: false,
        subscription_updating: false,
        help_requested: false,
      }
    : {}; // fallback to {} so that sub states don't return null

// ***********
// store
// ***********

// context that stores and shares data
const StoreContext = createContext();

// component to wrap upper level root component with Provider
export const StoreProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initState);

  return (
    <StoreContext.Provider value={{ state, dispatch }}>
      {children}
    </StoreContext.Provider>
  );
};

// useStore hook.  Acts as Consumer through useContext
const useStore = () => {
  const { state, dispatch } = useContext(StoreContext);
  return { state, dispatch };
};
export default useStore;
