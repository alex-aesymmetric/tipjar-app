import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useAppBridge, useClientRouting } from "@shopify/app-bridge-react";
import { NavigationMenu } from "@shopify/app-bridge-react";
import API from "../utils/api";
//import Intercom from "react-intercom";
// import components
import Loading from "./Loading";
// import actions / store
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

export default function Layout(props) {
  const { state, dispatch } = useStore();
  const router = useRouter();
  const appBridge = useAppBridge();
  // Add state for client-side rendering check
  const [isMounted, setIsMounted] = useState(false);

  // hook allows embedded app to page transition without full reload each time
  useClientRouting(router);

  // Navigation menu items
  const navigationItems = [
    {
      label: "Settings",
      destination: "/",
    },
    {
      label: "Installation",
      destination: "/install",
    },
    {
      label: "Account",
      destination: "/plan",
    },
    {
      label: "More Apps",
      destination: "/more-apps",
    },
  ];

  // Set isMounted to true when component mounts on client
  useEffect(() => {
    setIsMounted(true);
  }, []);

  // fetch initial shop data on component mount
  useEffect(() => {
    init();
  }, []);

  useEffect(() => {
    if (state?.shopDetails?.shop_domain) {
      initShopTag();
    }
  }, [state]);

  const init = async () => {
    if (typeof window !== "undefined") {
      window.appBridge = await appBridge;
      window.shopOrigin = await props.shopOrigin;
      ACTIONS.getShopData(state, dispatch);
    }
  };

  const initShopTag = async () => {
    if (typeof window !== "undefined") {
      if (state?.shopDetails?.shop_domain) {
        const res = await API({
          method: "POST",
          url: `/shop/script-tag`,
        });
      }
    }
  };

  // get intercom data
  // const intercomUser = {
  //   user_id: state.shopDetails?.id || "1234567890",
  //   email: state.shopDetails?.store_owner_email || "undefined@undefined.com",
  //   name: state.shopDetails?.store_owner_full_name || "Tip Quik User",
  //   created_at: Date.parse(state.shopDetails?.created_at) || 1598337229000,
  //   shopify_store_url:
  //     state.shopDetails?.shop_domain || "undefined.myshopify.com",
  //   app_name: "Tip Quik",
  // };

  return (
    <>
      {/* Only render NavigationMenu on client-side */}
      {isMounted && (
        <NavigationMenu
          navigationLinks={navigationItems}
          matcher={(link) => {
            return router.pathname === link.destination;
          }}
        />
      )}

      {/* if no state.shopDetails, display loading */}
      {!state.shopDetails || !state.shopSettings ? (
        <Loading />
      ) : (
        <>
          {props.children}
          {/*<Intercom appID={INTERCOM_APP_ID} {...intercomUser} />*/}
        </>
      )}
    </>
  );
}
