import React from "react";

export default function TipButton({ id, percentage, style }) {
  return (
    <span className="tj-modal-btn-wrapper">
      <button id={id} type="button" className="tj-modal-btn" style={style}>
        {/* shows the percentage */}
        <span className="tj-modal-btn-percentage">{percentage}%</span>
        {/* shows the dollar value */}
        <span id="tipQuikAmt1" className="tj-modal-btn-amount">
          ${percentage}
        </span>
      </button>
    </span>
  );
}
