import { useState } from "react";
import { Layout, SettingToggle } from "@shopify/polaris";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

function EnablePoweredTipQuik({ settings }) {
  const { state, dispatch } = useStore();

  const [isActive, isActive_set] = useState(settings.enable_powered_tip_quik);

  const handleToggle = () => {
    let newSettings = { ...settings, enable_powered_tip_quik: !isActive };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    isActive_set(!isActive);
  };

  // default enabled (current) on free plan
  const toggleTo = isActive ? "Disable" : "Enable";
  const current = isActive ? "enabled" : "disabled";

  return (
    <Layout.AnnotatedSection
      title="Enable 'Powered by Tip Quik' text"
      description={`Will show the 'Powered by Tip Quik' text at the bottom of the tip modal pop-up.`}
    >
      <SettingToggle
        action={{
          content: toggleTo,
          onAction: handleToggle,
          disabled: state.isFreePlan,
        }}
        enabled={isActive}
      >
        <p>
          'Powered by Tip Quik' text is currently <strong>{current}</strong>.
        </p>
        {state.isFreePlan && (
          <p>
            <em>Upgrade</em> to a paid plan to change this setting.
          </p>
        )}
      </SettingToggle>
    </Layout.AnnotatedSection>
  );
}

export default EnablePoweredTipQuik;
