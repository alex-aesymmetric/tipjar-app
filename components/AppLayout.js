import TopBannerInformation from "./TopBannerInformation";

function AppLayout({ children }) {
  return (
    <>
      <TopBannerInformation />
      {children}
    </>
  );
}

export default AppLayout;
