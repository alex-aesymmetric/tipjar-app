import { FooterHelp, Link } from "@shopify/polaris";
// import utils
//import { triggerIntercom } from "../utils/triggerIntercom";

function FooterHelpDiv() {
  return (
    <FooterHelp>
      Need help? Contact us via email at
      <span className="install-email">
        <Link external url="mailto:support@aesymmetric.xyz">
          support@aesymmetric.xyz
        </Link>
      </span>
    </FooterHelp>
  );
}

export default FooterHelpDiv;
