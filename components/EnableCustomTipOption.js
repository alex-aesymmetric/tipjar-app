import { useState } from "react";
import { Layout, SettingToggle } from "@shopify/polaris";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

function EnableCustomTipOption({ settings }) {
  const { state, dispatch } = useStore();

  const [isActive, isActive_set] = useState(settings.enable_custom_tip_option);

  const handleToggle = () => {
    let newSettings = { ...settings, enable_custom_tip_option: !isActive };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    isActive_set(!isActive);
  };

  // default disabled (current) on free plan
  const toggleTo = isActive ? "Disable" : "Enable";
  const current = isActive ? "enabled" : "disabled";

  return (
    <Layout.AnnotatedSection
      title="Enable 'Custom Tip Amount' option"
      description={`Will enable or disable the 'Custom Tip Amount' option on the tip modal pop-up.`}
    >
      <SettingToggle
        action={{
          content: toggleTo,
          onAction: handleToggle,
          disabled: state.isFreePlan,
        }}
        enabled={isActive}
      >
        <p>
          Custom Tip Amount is currently <strong>{current}</strong>.
        </p>
        {state.isFreePlan && (
          <p>
            <em>Upgrade</em> to a paid plan to change this setting.
          </p>
        )}
      </SettingToggle>
    </Layout.AnnotatedSection>
  );
}

export default EnableCustomTipOption;
