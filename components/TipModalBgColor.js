import { useState } from "react";
import {
  Layout,
  Card,
  FormLayout,
  TextField,
  ColorPicker,
} from "@shopify/polaris";
import colorsys from "colorsys";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

const hsbToHsv = function ({ hue: h, brightness: v, saturation: s }) {
  const hsv = { h, s: s * 100, v: v * 100 };
  return hsv;
};

const hsvToHsb = function ({ h: hue, v: brightness, s: saturation }) {
  const hsb = {
    hue,
    brightness: brightness / 100,
    saturation: saturation / 100,
  };
  return hsb;
};

function TipModalBgColor({ settings }) {
  const { state, dispatch } = useStore();

  const [bgColor, bgColor_set] = useState(
    hsvToHsb(colorsys.hexToHsv(settings.tip_modal_bg_color))
  );

  const handleChange = (bgColor_NEW) => {
    let newSettings = {
      ...settings,
      tip_modal_bg_color: colorsys.hsvToHex(hsbToHsv(bgColor_NEW)),
    };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    bgColor_set(bgColor_NEW);
  };

  return (
    <Layout.AnnotatedSection
      title="Tip modal background color"
      description="This is the background color that will be used for the tip modal pop-up."
    >
      <Card sectioned>
        <FormLayout>
          <ColorPicker onChange={handleChange} color={bgColor} />
        </FormLayout>
      </Card>
    </Layout.AnnotatedSection>
  );
}

export default TipModalBgColor;
