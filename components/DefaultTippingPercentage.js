import { useState, useEffect } from "react";
import {
  Layout,
  Card,
  FormLayout,
  TextField,
  TextContainer,
} from "@shopify/polaris";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

function DefaultTippingPercentage({ settings, emptyError }) {
  const { state, dispatch } = useStore();

  const [tip_percent_1, tip_percent_1_set] = useState(settings?.tip_percent_1);
  const [tip_percent_2, tip_percent_2_set] = useState(settings?.tip_percent_2);
  const [tip_percent_3, tip_percent_3_set] = useState(settings?.tip_percent_3);

  const handleChange1 = (newValue) => {
    if (newValue > 100 || newValue < 1) {
      return settings.tip_percent_1;
    }
    const newSettings = { ...settings, tip_percent_1: parseInt(newValue) };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    tip_percent_1_set(newValue);
  };

  const handleChange2 = (newValue) => {
    if (newValue > 100 || newValue < 1) {
      return settings.tip_percent_2;
    }
    const newSettings = { ...settings, tip_percent_2: parseInt(newValue) };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    tip_percent_2_set(newValue);
  };

  const handleChange3 = (newValue) => {
    if (newValue > 100 || newValue < 1) {
      return settings.tip_percent_3;
    }
    const newSettings = { ...settings, tip_percent_3: parseInt(newValue) };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    tip_percent_3_set(newValue);
  };

  return (
    <Layout.AnnotatedSection
      title="Preset tip amount percentages"
      description="Set the tip percentage numbers that are displayed by default on the tip modal pop-up."
    >
      <Card sectioned>
        <FormLayout>
          <FormLayout.Group condensed>
            <TextField
              label="Tip Amount 1"
              id="tip_percent_1"
              type="number"
              suffix="%"
              min={1}
              max={100}
              value={tip_percent_1.toString()}
              onChange={handleChange1}
            />
            <TextField
              label="Tip Amount 2"
              id="tip_percent_2"
              type="number"
              suffix="%"
              min={1}
              max={100}
              value={tip_percent_2.toString()}
              onChange={handleChange2}
            />
            <TextField
              label="Tip Amount 3"
              id="tip_percent_1"
              type="number"
              suffix="%"
              min={1}
              max={100}
              value={tip_percent_3.toString()}
              onChange={handleChange3}
            />
          </FormLayout.Group>
        </FormLayout>
        {emptyError && (
          <TextContainer>
            <p className="input-validation">
              You are trying with empty value. Please check and retry.
            </p>
          </TextContainer>
        )}
      </Card>
    </Layout.AnnotatedSection>
  );
}

export default DefaultTippingPercentage;
