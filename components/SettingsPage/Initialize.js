import { Layout, Card } from "@shopify/polaris";
// import store / actions
import useStore from "../../store/useStore";
import * as ACTIONS from "../../store/actions";

// ***********
// component
// ***********

export default function Initialize({ defaultSettingsLoading }) {
  const { state, dispatch } = useStore();

  return (
    <Layout>
      <Layout.Section>
        <Card
          title="Thanks for installing Tip Quik!"
          primaryFooterAction={{
            content: "Set up initial settings",
            loading: state.metafields_installing,
            onAction: () => ACTIONS.installSettings_shop(state, dispatch),
          }}
        >
          <Card.Section>
            Press the button below to set up the initial Tip Quik settings.
          </Card.Section>
        </Card>
      </Layout.Section>
    </Layout>
  );
}
