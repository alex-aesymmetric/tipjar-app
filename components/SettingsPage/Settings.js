import React from "react";
import { useState, useEffect } from "react";
import { Page, Layout, FormLayout, Card, PageActions } from "@shopify/polaris";
// import utils
import { isEqual } from "lodash";
// import components
import DefaultTippingPercentage from "../DefaultTippingPercentage";
import EnableCustomTipOption from "../EnableCustomTipOption";
import EnablePoweredTipQuik from "../EnablePoweredTipQuik";
import EnableTipQuikApp from "../EnableTipQuikApp";
import TipButton from "../TipButton";
import TipModalBgColor from "../TipModalBgColor";
import TipModalDescription from "../TipModalDescription";
import TipModalTextColor from "../TipModalTextColor";
import TipModalTitle from "../TipModalTitle";
// import store / actions
import useStore from "../../store/useStore";
import * as ACTIONS from "../../store/actions";

// ***********
// component
// ***********

export default function Settings({ settings }) {
  const { state, dispatch } = useStore();

  const [buttonClicked, buttonClicked_set] = useState(false);
  const [customValue, customValue_set] = useState(0);

  // function handles key presses
  async function keyPress_handle(event) {
    return event.charCode >= 48 && event.charCode <= 57;
  }

  // function handles changes
  async function customChange_handle(event) {
    const targetValue = event.target.value;
    if (event.target.value < 1000) {
      customValue_set(targetValue);
    }
  }

  const tjModalContentStyle = {
    backgroundColor: settings?.tip_modal_bg_color,
    color: settings?.tip_modal_text_color,
  };
  const tjModalTitleStyle = {
    color: settings?.tip_modal_text_color,
  };
  const tjModalDescriptionStyle = {
    color: settings?.tip_modal_text_color,
  };
  const tjModalSuccessContentStyle = {
    backgroundColor: settings?.tip_modal_bg_color,
  };
  const tipQuikBtnCustomStyle = {
    display: buttonClicked ? "none" : "block",
    color: settings?.tip_modal_text_color,
  };
  const tipQuikCustomInputWrapperStyle = {
    display: buttonClicked ? "flex" : "none",
    color: settings?.tip_modal_text_color,
  };

  // const handleCustom = () => {
  //   buttonClicked_set(true);
  // };

  return (
    <Layout>
      {/* <EnableTipQuikApp settings={settings} /> */}
      <DefaultTippingPercentage settings={settings} />
      <EnableCustomTipOption settings={settings} />
      <EnablePoweredTipQuik settings={settings} />
      <TipModalTitle settings={settings} />
      <TipModalDescription settings={settings} />
      <TipModalBgColor settings={settings} />
      <TipModalTextColor settings={settings} />
      <Layout.AnnotatedSection
        title="Tip modal preview"
        description="This is a preview of how the tip modal pop-up will appear to your customers on the cart page."
      >
        <FormLayout>
          <div id="tipQuikModal" className="tj-modal">
            <div className="tj-modal-background">
              <div className="tj-modal-background-inner"></div>
            </div>
            <div
              className="tj-modal-content-wrapper"
              style={tjModalDescriptionStyle}
            >
              <div className="tj-modal-content" style={tjModalContentStyle}>
                <div className="tj-modal-header">
                  <div className="tj-modal-header-inner">
                    <h3 className="tj-modal-title" style={tjModalTitleStyle}>
                      {settings?.tip_modal_title}
                    </h3>
                    <p
                      className="tj-modal-description"
                      style={tjModalDescriptionStyle}
                    >
                      {settings?.tip_modal_description}
                    </p>
                  </div>
                </div>
                <div
                  className="tj-modal-btns-container"
                  style={tjModalDescriptionStyle}
                >
                  <TipButton
                    id="tipQuikBtn1"
                    percentage={settings?.tip_percent_1}
                    style={tjModalDescriptionStyle}
                  />
                  <TipButton
                    id="tipQuikBtn2"
                    percentage={settings?.tip_percent_2}
                    style={tjModalDescriptionStyle}
                  />
                  <TipButton
                    id="tipQuikBtn3"
                    percentage={settings?.tip_percent_3}
                    style={tjModalDescriptionStyle}
                  />
                </div>

                {/* default disabled on free plan */}
                {settings?.enable_custom_tip_option && !state.isFreePlan && (
                  <span className="tj-modal-btn-wrapper">
                    <button
                      id="tipQuikBtnCustom"
                      type="button"
                      className="tj-modal-btn"
                      // onClick={handleCustom}
                      style={tipQuikBtnCustomStyle}
                    >
                      <span className="tj-modal-btn-percentage">
                        Custom amount
                      </span>
                    </button>

                    <span
                      id="tipQuikCustomInputWrapper"
                      className="tj-modal-input-wrapper"
                      style={tipQuikCustomInputWrapperStyle}
                    >
                      <input
                        id="tipQuikCustomInput"
                        className="tj-modal-custom-input"
                        type="number"
                        value={customValue}
                        min="0"
                        max="1000"
                        onChange={customChange_handle}
                        onKeyPress={keyPress_handle}
                        style={tjModalDescriptionStyle}
                      />
                      <button
                        id="tipQuikCustomInputAdd"
                        className="tj-modal-btn tj-modal-btn-percentage tj-modal-input-add"
                        type="button"
                        data-tipquik-add="0"
                        style={tjModalDescriptionStyle}
                      >
                        Add
                      </button>
                    </span>
                  </span>
                )}

                <span className="tj-modal-btn-none">
                  <button
                    type="button"
                    className="tj-modal-btn"
                    data-tipquik-add="0"
                    style={tipQuikBtnCustomStyle}
                  >
                    <span className="tj-modal-btn-percentage">No tip</span>
                  </button>
                </span>

                {/* default enabled on free plan */}
                {(settings?.enable_powered_tip_quik || state.isFreePlan) && (
                  <span className="tj-modal-powered">Powered by Tip Quik</span>
                )}

                <div
                  id="tipQuikSuccess"
                  className="tj-modal-success-content"
                  style={tjModalSuccessContentStyle}
                >
                  <div>
                    <div className="tj-modal-loading-icon-container">
                      <svg
                        className="tj-modal-loading-icon"
                        viewBox="0 0 44 44"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path d="M15.542 1.487A21.507 21.507 0 00.5 22c0 11.874 9.626 21.5 21.5 21.5 9.847 0 18.364-6.675 20.809-16.072a1.5 1.5 0 00-2.904-.756C37.803 34.755 30.473 40.5 22 40.5 11.783 40.5 3.5 32.217 3.5 22c0-8.137 5.3-15.247 12.942-17.65a1.5 1.5 0 10-.9-2.863z"></path>
                      </svg>
                    </div>
                    <p className="tj-modal-success-title">Thank you</p>
                    <p className="tj-modal-success-message">
                      You are now being directed to the checkout page.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </FormLayout>
      </Layout.AnnotatedSection>
    </Layout>
  );
}
