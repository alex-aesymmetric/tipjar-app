import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Banner, Stack } from "@shopify/polaris";
import useStore from "../store/useStore";
import AesymmetricBanner from "./AesymmetricBanner";

function TopBannerInformation() {
  const { state } = useStore();
  const router = useRouter();

  const isInstallationComplete = state.product_created;

  return (
    <Stack vertical>
      <Banner status="info">
        <p>
          December 30 Update - We have recently fixed an issue with the app. If
          for some reason the pop-up is not correctly working on your store,
          please try updating your app settings and re-saving. If that does not
          work try re-installing the app. If the app still does not work after
          these steps, please reach out to us for additional assistance.
        </p>
      </Banner>

      {!isInstallationComplete && (
        <Banner
          title="You need to finish installing Tip Quik"
          action={{
            content: "Finish installation",
            onAction: () => router.push(`/install`),
          }}
          status="info"
        >
          <p>
            Tip Quik will not begin working on your store until you finish all
            of the installation steps. Please click on the 'Install' link on the
            left side menu.
          </p>
        </Banner>
      )}

      {isInstallationComplete && <AesymmetricBanner />}
    </Stack>
  );
}

export default TopBannerInformation;
