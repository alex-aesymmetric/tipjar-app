import { useState } from "react";
import {
  Layout,
  Card,
  FormLayout,
  TextField,
  ColorPicker,
} from "@shopify/polaris";
import colorsys from "colorsys";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ********
// helpers
// ********

const hsbToHsv = function ({ hue: h, brightness: v, saturation: s }) {
  const hsv = { h, s: s * 100, v: v * 100 };
  return hsv;
};

const hsvToHsb = function ({ h: hue, v: brightness, s: saturation }) {
  const hsb = {
    hue,
    brightness: brightness / 100,
    saturation: saturation / 100,
  };
  return hsb;
};

// ***********
// component
// ***********

function TipModalTextColor({ settings }) {
  const { state, dispatch } = useStore();

  const [textColor, textColor_set] = useState(
    hsvToHsb(colorsys.hexToHsv(settings.tip_modal_text_color))
  );

  const handleChange = (newColor) => {
    let newSettings = {
      ...settings,
      tip_modal_text_color: colorsys.hsvToHex(hsbToHsv(newColor)),
    };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    textColor_set(newColor);
  };

  return (
    <Layout.AnnotatedSection
      title="Tip modal text color"
      description="This is the text color that will be used for the tip modal pop-up."
    >
      <Card sectioned>
        <FormLayout>
          <ColorPicker onChange={handleChange} color={textColor} />
        </FormLayout>
      </Card>
    </Layout.AnnotatedSection>
  );
}

export default TipModalTextColor;
