import NextHead from "next/head";

export default function Head({ title, path }) {
  return (
    <NextHead>
      <script
        async
        src={`https://www.googletagmanager.com/gtag/js?id=${ANALYTIC_KEY}`}
      ></script>
      <script
        dangerouslySetInnerHTML={{
          __html: `
          window.dataLayer = window.dataLayer || [];
          function gtag(){window.dataLayer.push(arguments)}
          gtag('js', new Date());

          gtag('config', "${ANALYTIC_KEY}", {'page_title': 'Account', 'page_path': '/plan'});
        `,
        }}
      />
    </NextHead>
  );
}
