import React from "react";
import { Page, Spinner } from "@shopify/polaris";

// ***********
// component
// ***********

export default function Loading({ children }) {
  return (
    <Page>
      <Spinner accessibilityLabel="Loading spinner" />
    </Page>
  );
}
