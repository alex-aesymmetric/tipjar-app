import { useState, useCallback } from "react";
import { Layout, Card, FormLayout, TextField } from "@shopify/polaris";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

function TipModalDescription({ settings }) {
  const { state, dispatch } = useStore();

  const [description, description_set] = useState(
    settings.tip_modal_description
  );

  const handleChange = (newDescription) => {
    let newSettings = {
      ...settings,
      tip_modal_description: newDescription,
    };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    description_set(newDescription);
  };

  return (
    <Layout.AnnotatedSection
      title="Tip modal description"
      description="This is the description text that will be displayed on the tip modal pop-up."
    >
      <Card sectioned>
        <FormLayout>
          <TextField
            label=""
            value={description}
            onChange={handleChange}
            multiline={true}
          />
        </FormLayout>
      </Card>
    </Layout.AnnotatedSection>
  );
}

export default TipModalDescription;
