import { useState } from "react";
import { Layout, Card, FormLayout, TextField } from "@shopify/polaris";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

function TipModalTitle({ settings }) {
  const { state, dispatch } = useStore();

  const [title, title_set] = useState(settings.tip_modal_title);

  const handleChange = (newTitle) => {
    let newSettings = { ...settings, tip_modal_title: newTitle };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    title_set(newTitle);
  };

  return (
    <Layout.AnnotatedSection
      title="Tip modal title"
      description="This is the title text that will be displayed on tip modal pop-up."
    >
      <Card sectioned>
        <FormLayout>
          <TextField label="" value={title} onChange={handleChange} />
        </FormLayout>
      </Card>
    </Layout.AnnotatedSection>
  );
}

export default TipModalTitle;
