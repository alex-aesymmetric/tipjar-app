import { Banner, Link } from "@shopify/polaris";
import { useRouter } from "next/router";

function AesymmetricBanner() {
  return (
    <div className="aesymmetric-banner">
      <Banner>
        <p>
          Increase your store's AOV, CVR, and LTV with a tailor made strategy
          created for your brand. 💸{" "}
          <Link url={`https://aesymmetric.xyz/?r=tip-quik`} external monochrome>
            Click here to learn more.
          </Link>
        </p>
      </Banner>

      <style jsx>{`
        .aesymmetric-banner :global(.Polaris-Banner) {
          animation: glow 2s infinite;
          border: 2px solid #ffd700;
          border-radius: 4px;
          padding: 1px;
        }

        @keyframes glow {
          0% {
            box-shadow: 0 0 5px #ffd700;
          }
          50% {
            box-shadow: 0 0 20px #ffd700, 0 0 30px #ffa500;
          }
          100% {
            box-shadow: 0 0 5px #ffd700;
          }
        }
      `}</style>
    </div>
  );
}

export default AesymmetricBanner;
