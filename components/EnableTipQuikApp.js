import { useState } from "react";
import { Layout, SettingToggle } from "@shopify/polaris";
// import store / actions
import useStore from "../store/useStore";
import * as ACTIONS from "../store/actions";

// ***********
// component
// ***********

function EnableTipQuikApp({ settings }) {
  const { state, dispatch } = useStore();

  const [isActive, isActive_set] = useState(settings.enable_tip_quik);

  const handleToggle = () => {
    let newSettings = { ...settings, enable_tip_quik: !isActive };
    ACTIONS.updateSettings(state, dispatch, newSettings);
    isActive_set(!isActive);
  };

  const contentStatus = isActive ? "Disable" : "Enable";
  const textStatus = isActive ? "enabled" : "disabled";

  return (
    <Layout.AnnotatedSection
      title="Enable Tip Quik app"
      description="Will enable or disable the Tip Quik tip modal pop-up from showing up on the store's cart page."
    >
      <SettingToggle
        action={{
          content: contentStatus,
          onAction: handleToggle,
        }}
        enabled={isActive}
      >
        Tip Quik app is currently <strong>{textStatus}</strong>.
      </SettingToggle>
    </Layout.AnnotatedSection>
  );
}

export default EnableTipQuikApp;
