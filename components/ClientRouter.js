import { useRouter } from "next/router";
import { ClientRouter as AppBridgeClientRouter } from "@shopify/app-bridge-react";

function ClientRouter(props) {
  const router = useRouter();
  return <AppBridgeClientRouter history={router} />;
}

export default ClientRouter;
