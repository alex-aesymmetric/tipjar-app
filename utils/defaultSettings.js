export default {
  enable_tip_quik: false,
  enable_custom_tip_option: false,
  enable_powered_tip_quik: true,
  tip_modal_title: "Do you want to leave a tip?",
  tip_modal_description: "All tips go directly to our hard working employees.",
  tip_percent_1: 15,
  tip_percent_2: 20,
  tip_percent_3: 25,
  tip_modal_text_color: "#000000",
  tip_modal_bg_color: "#ffffff",
};
