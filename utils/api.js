import "url-search-params-polyfill";
import axios from "axios";
// import app bridge vars / functions
import { getSessionToken } from "@shopify/app-bridge-utils";

const instance = axios.create();

// intercept all requests on this axios instance
instance.interceptors.request.use(async (config) => {
  // requires an App Bridge instance
  const token = await getSessionToken(window.appBridge);

  // append your request headers with an authenticated token
  config.headers["authorization"] = `Bearer ${token}`;

  // Add specific headers for GraphQL requests
  if (config.url === "/graphql") {
    config.headers["Content-Type"] = "application/json";
    config.headers["Accept"] = "application/json";

    // Transform the data to match Shopify's GraphQL API expectations
    if (config.data) {
      config.data = {
        query: config.data.query,
        variables: config.data.variables || {},
      };
    }
  }

  // append shop param to each request
  config.params = {
    shop: window.shopOrigin,
  };

  return config;
});

// Add response interceptor to handle GraphQL responses
instance.interceptors.response.use(
  (response) => {
    // If it's a GraphQL response, check for errors
    if (response.config.url === "/graphql") {
      const data = response.data;

      if (data.errors) {
        return Promise.reject({
          response: {
            data: data.errors,
            status: 400,
          },
        });
      }
    }
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// export your axios instance to use within your app
export default instance;
