const express = require("express");
const router = express.Router();
const { handleAppUninstalled, handleAppInstalled } = require("./controllers");

router.post("/webhooks/app/uninstalled", handleAppUninstalled);
router.post("/webhooks/app/installed", handleAppInstalled);
