export async function handleAppInstalled(ctx) {
  try {
    const { shop_domain } = ctx.state.webhook;
    const shop = await services.getShopByDomain(shop_domain);

    if (!shop) {
      throw new Error(`Shop ${shop_domain} not found`);
    }

    // Create initial metafields
    await services_meta.createInitialMetafields(
      ctx,
      shop_domain,
      shop.access_token
    );

    ctx.status = 200;
  } catch (error) {
    console.error("App install webhook error:", error);
    ctx.status = 500;
  }
}
