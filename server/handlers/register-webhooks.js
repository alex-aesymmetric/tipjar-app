import { registerWebhook } from "@shopify/koa-shopify-webhooks";

export const registerWebhooks = async (
  url,
  type,
  accessToken,
  shop,
  apiVersion
) => {
  const registration = await registerWebhook({
    address: `${process.env.HOST}${url}`,
    topic: type,
    accessToken,
    shop,
    apiVersion,
  });

  if (registration.success) {
    console.log("Successfully registered webhook!", type);
  } else {
    console.error(
      "Failed to register webhook",
      registration.result.data.webhookSubscriptionCreate
    );
  }
};
