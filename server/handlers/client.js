import { ApolloClient } from "@apollo/client";

export const createClient = (shop, accessToken) => {
  return new ApolloClient({
    uri: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
    request: (operation) => {
      operation.setContext({
        headers: { "X-Shopify-Access-Token": accessToken },
      });
    },
  });
};
