import "isomorphic-fetch";
import axios from "axios";
import { db } from "./db";
// import helpers / services
import * as controllers_subs from "./subscriptions/controllers";
import * as services from "./services";
import * as services_meta from "./metafields/services";
import * as services_subs from "./subscriptions/services";
import { getCurrentDate } from "./utils";
import https from "https";

// ****************
// controller
// ****************

// function gets shop settings from DATABASE
export async function getShopDetails_db(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  ctx.status = 200;
  ctx.body = shopDetails_db;
  return;
}

// ****************
// controller
// ****************

// function gets shop settings from DATABASE
export async function getShopSettings_db(ctx) {
  const shopSettings_db = await services.getShopSettings_db(ctx);

  ctx.status = 200;
  ctx.body = shopSettings_db;
  return;
}

// ****************
// controller
// ****************

// function fetches shop details from SHOPIFY admin api
export async function fetchShopDetails_shopify(ctx) {
  const shopDetails_shopify = await services.fetchShopDetails_shopify(ctx);

  ctx.status = 200;
  ctx.body = shopDetails_shopify;
  return;
}

// ****************
// controller
// ****************

// function creates a snippet
export async function createSnippet(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { access_token, shop_domain: shop } = shopDetails_db;
  const { asset } = ctx.request.body;

  // create script tag on store
  const res_scriptTag = await axios({
    method: "POST",
    url: `https://${shop}/admin/api/${process.env.API_VERSION}/script_tags.json`,
    headers: { "X-Shopify-Access-Token": access_token },
    data: {
      script_tag: {
        event: `onload`,
        src: `${process.env.HOST}/script`,
      },
    },
  });
  // console.log(`...script tag created`); // ? debug

  // get published theme
  const res_getThemes = await axios({
    method: "GET",
    url: `https://${shop}/admin/api/${process.env.API_VERSION}/themes.json`,
    headers: { "X-Shopify-Access-Token": access_token },
  });
  const getThemes = await res_getThemes.data;

  const publishedTheme = getThemes.themes.find(
    (theme) => theme.role === "main"
  );
  const publishedTheme_id = publishedTheme.id;

  // return message if no snippet value provided
  if (!asset) {
    ctx.body = "No asset value or themeId provided.";
  }

  // add snippet to published theme
  const res_createSnippet = await axios({
    method: "PUT",
    url: `https://${shop}/admin/api/${process.env.API_VERSION}/themes/${publishedTheme_id}/assets.json`,
    headers: { "X-Shopify-Access-Token": access_token },
    data: { asset },
  });
  const createSnippet = await res_createSnippet.data;
  // console.log(`...snippet added to main theme`); // ? debug

  const updateSnippet_db = await db.query(
    `UPDATE shops SET 
      snippet_installation_status=$1, 
      updated_at=$2 
    WHERE 
      shop_domain=$3`,
    [true, getCurrentDate(), shop]
  );

  ctx.body = getThemes;
}

// ****************
// controller
// ****************

// function get shop themes
export async function shopThemes(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { access_token, shop_domain: shop } = shopDetails_db;
  const res_getThemes = await axios({
    method: "GET",
    url: `https://${shop}/admin/api/${process.env.API_VERSION}/themes.json`,
    headers: { "X-Shopify-Access-Token": access_token },
  });
  const getThemes = await res_getThemes.data;
  ctx.body = getThemes.themes;
}

export async function scriptTagGet(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { access_token, shop_domain: shop } = shopDetails_db;
  const res_scriptTagGet = await axios({
    method: "GET",
    url: `https://${shop}/admin/api/${process.env.API_VERSION}/script_tags.json`,
    headers: { "X-Shopify-Access-Token": access_token },
  });
  const scriptTag = await res_scriptTag.data;
  ctx.body = scriptTag;
}

// ****************
// controller
// ****************

// function set script tag
export async function scriptTagPost(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { access_token, shop_domain: shop } = shopDetails_db;
  let isScriptTag = false;
  let scriptTag = null;
  const res_scriptTagGet = await axios({
    method: "GET",
    url: `https://${shop}/admin/api/${process.env.API_VERSION}/script_tags.json`,
    headers: { "X-Shopify-Access-Token": access_token },
  });
  const scriptTagGet = await res_scriptTagGet.data;
  if (scriptTagGet.script_tags) {
    for (let i = 0; i < scriptTagGet.script_tags.length; i++) {
      for (let j = 0; j < scriptTagGet.script_tags.length; j++) {
        if (
          scriptTagGet.script_tags[i].src === scriptTagGet.script_tags[j].src &&
          scriptTagGet.script_tags[i].event ===
            scriptTagGet.script_tags[j].event
        ) {
          isScriptTag = true;
        }
      }
    }
  }
  if (!isScriptTag) {
    const res_scriptTag = await axios({
      method: "POST",
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/script_tags.json`,
      headers: { "X-Shopify-Access-Token": access_token },
      data: {
        script_tag: {
          event: `onload`,
          src: `${process.env.HOST}/script`,
        },
      },
    });
    scriptTag = await res_scriptTag.data;
    ctx.body = scriptTag;
  } else {
    scriptTag = await scriptTagGet.data;
    ctx.body = scriptTag;
  }
}

// ****************
// controller
// ****************

// function creates a product
export async function createProduct(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { access_token, shop_domain: shop } = shopDetails_db;

  try {
    // First, create the product
    const createProductQuery = {
      query: `
        mutation productCreate($input: ProductInput!) {
          productCreate(input: $input) {
            product {
              id
              title
              variants(first: 1) {
                nodes {
                  id
                }
              }
            }
            userErrors {
              field
              message
            }
          }
        }
      `,
      variables: {
        input: {
          title: "Tip/Gratuity",
          handle: "tip-gratuity",
          vendor: process.env.APP_NAME,
          status: "ACTIVE",
          productOptions: [
            {
              name: "Title",
              values: [
                {
                  name: "Default Title",
                },
              ],
            },
          ],
        },
      },
    };

    const createProductRes = await axios({
      method: "POST",
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      headers: { "X-Shopify-Access-Token": access_token },
      data: createProductQuery,
    });

    if (createProductRes.data.errors) {
      throw new Error(
        `Failed to create product: ${createProductRes.data.errors[0].message}`
      );
    }

    const productCreateData = createProductRes.data.data.productCreate;
    if (
      productCreateData.userErrors &&
      productCreateData.userErrors.length > 0
    ) {
      throw new Error(productCreateData.userErrors[0].message);
    }

    const productId = productCreateData.product.id;
    const variantId = productCreateData.product.variants.nodes[0].id;

    // Update the variant with specific settings
    const variantUpdateQuery = {
      query: `
        mutation productVariantsBulkUpdate($productId: ID!, $variants: [ProductVariantsBulkInput!]!) {
          productVariantsBulkUpdate(productId: $productId, variants: $variants) {
            product {
              id
            }
            productVariants {
              id
            }
            userErrors {
              field
              message
            }
          }
        }
      `,
      variables: {
        productId: productId,
        variants: [
          {
            id: variantId,
            price: "0.01",
            taxable: false,
            sku: "TIPQUIK-TG",
            inventoryPolicy: "CONTINUE",
            inventoryItem: {
              requiresShipping: false,
              tracked: false,
            },
            optionValues: [
              {
                optionName: "Title",
                name: "Default Title",
              },
            ],
          },
        ],
      },
    };

    try {
      const variantUpdateRes = await axios({
        method: "POST",
        url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
        headers: {
          "X-Shopify-Access-Token": access_token,
          "Content-Type": "application/json",
        },
        data: variantUpdateQuery,
      });

      // Log the full response for debugging
      console.log(
        "Variant Update Response:",
        JSON.stringify(variantUpdateRes.data, null, 2)
      );

      if (variantUpdateRes.data.errors) {
        console.error("GraphQL Errors:", variantUpdateRes.data.errors);
        throw new Error(
          `Failed to update product variant: ${variantUpdateRes.data.errors[0].message}`
        );
      }

      const userErrors =
        variantUpdateRes.data.data?.productVariantsBulkUpdate?.userErrors;
      if (userErrors && userErrors.length > 0) {
        console.error("User Errors:", userErrors);
        throw new Error(
          `Failed to update product variant: ${userErrors[0].message}`
        );
      }
    } catch (error) {
      console.error("Variant Update Error Details:", {
        status: error.response?.status,
        statusText: error.response?.statusText,
        data: error.response?.data,
        headers: error.response?.headers,
      });

      // Check if this is an authorization error
      if (error.response?.status === 403) {
        throw new Error(
          "Authorization failed. Please check app permissions and access token."
        );
      }

      throw error;
    }

    // Get online store publication ID
    const publicationQuery = {
      query: `
        {
          publications(first: 100) {
            edges {
              node {
                id
                name
              }
            }
          }
        }
      `,
    };

    const publicationRes = await axios({
      method: "POST",
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      headers: { "X-Shopify-Access-Token": access_token },
      data: publicationQuery,
    });

    if (publicationRes.data.errors) {
      throw new Error(
        `Failed to get publication ID: ${publicationRes.data.errors[0].message}`
      );
    }

    // Find the Online Store publication
    const onlineStorePublication = publicationRes.data.data.publications.edges.find(
      (edge) => edge.node.name === "Online Store"
    );

    if (!onlineStorePublication) {
      throw new Error("Cannot find Online Store sales channel");
    }

    const onlineStorePublicationId = onlineStorePublication.node.id;

    // Publish the product
    const publishQuery = {
      query: `
        mutation publishablePublish($id: ID!, $input: [PublicationInput!]!) {
          publishablePublish(id: $id, input: $input) {
            publishable {
              ... on Product {
                id
                title
                status
              }
            }
            userErrors {
              field
              message
            }
          }
        }
      `,
      variables: {
        id: productId,
        input: [
          {
            publicationId: onlineStorePublicationId,
            publishDate: new Date().toISOString(),
          },
        ],
      },
    };

    const publishRes = await axios({
      method: "POST",
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      headers: { "X-Shopify-Access-Token": access_token },
      data: publishQuery,
    });

    if (
      publishRes.data.errors ||
      (publishRes.data.data.publishablePublish.userErrors &&
        publishRes.data.data.publishablePublish.userErrors.length > 0)
    ) {
      throw new Error("Failed to publish product");
    }

    // Update database
    const updatedShop = await db.query(
      `UPDATE shops SET 
        product_installation_status=$1, 
        updated_at=$2 
      WHERE 
        shop_domain=$3
      RETURNING *`,
      [true, getCurrentDate(), shop]
    );

    // Return both the product data and the updated shop status
    ctx.body = {
      product: productCreateData.product,
      shop: updatedShop.rows[0],
    };
    ctx.status = 200;
  } catch (error) {
    console.error("Error creating product:", error);
    ctx.status = 500;
    ctx.body = { error: error.message };
  }
}

// ****************
// controller
// ****************

// ! currently inactive
// function requests help
export async function requestHelp(ctx) {
  const { store_data } = ctx.request.body;
  // Send help request mail to support email (support@aesymmetric.xyz)
  try {
    const sgMail = require("@sendgrid/mail");
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    sgMail.send({
      to: process.env.SUPPORT_EMAIL,
      from: process.env.SUPPORT_EMAIL,
      subject: `Tip Quik - installation help request - ${store_data.shop_domain}`,
      text: `
          Tip Quik app installation help has been requested. \n 
          Store URL: ${store_data.shop_domain} \n 
          Store owner email: ${store_data.shop_owner}
        `,
      html: `
          <p>Tip Quik app installation help has been requested.</p>
          <p>Store URL: 
            <a href="${store_data.shop_domain}">${store_data.shop_domain}</a>
          </p>
          <p> Store owner email: ${store_data.shop_owner}</p>
        `,
    });
  } catch (error) {
    console.error("sendgrid error: ", error);
  }

  // Change installation_help_status in shop table (psql)
  const updateShop = await db.query(
    "UPDATE shops SET installation_help_status=true, updated_at=$1 WHERE shop_domain=$2 RETURNING *",
    [getCurrentDate(), store_data.shop_domain]
  );
  ctx.body = { store_data: updateShop.rows[0] };
}

// ****************
// controller
// ****************

// function uninstalls a shop from the app
export async function uninstallShop(ctx) {
  const { webhook } = ctx.state;
  const shopDetails_db = await services.getShopDetails_db(ctx, webhook);
  const { id: shop_id, store_owner_email } = shopDetails_db;
  const subscription_status = "UNINSTALLED";

  // console.log("received webhook: APP_UNINSTALLED"); // ? debug

  // set app uninstallation time in DATABASE
  await services.uninstall_app_db(ctx, webhook, subscription_status);

  // Send Slack notification for app uninstallation
  try {
    await sendSlackNotification(
      process.env.ZAPIER_SLACK_WEBHOOK_URL,
      webhook.domain,
      store_owner_email,
      ctx,
      "app_uninstall"
    );
  } catch (error) {
    console.error("Failed to send Slack notification:", error);
    // Note: We don't throw an error here to avoid disrupting the uninstallation process
  }
}

// ****************
// controller
// ****************

// function subscribes to klaviyo
export async function klaviyoSubscribe(apiKey, listId, shopDetails, ctx) {
  try {
    // First, try to get the existing profile
    const getProfile = await fetch(
      `https://a.klaviyo.com/api/profiles/?filter=equals(email,"${encodeURIComponent(
        shopDetails.email
      )}")`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Klaviyo-API-Key ${apiKey}`,
          revision: "2023-02-22",
        },
      }
    );

    let profileId;
    if (getProfile.ok) {
      const profileData = await getProfile.json();
      if (profileData.data.length > 0) {
        profileId = profileData.data[0].id;
        console.log("Existing profile found, updating...");
      }
    }

    let profileResponse;
    if (profileId) {
      // Update existing profile
      profileResponse = await fetch(
        `https://a.klaviyo.com/api/profiles/${profileId}`,
        {
          method: "PATCH",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Klaviyo-API-Key ${apiKey}`,
            revision: "2023-02-22",
          },
          body: JSON.stringify({
            data: {
              type: "profile",
              id: profileId,
              attributes: {
                email: shopDetails.email,
                properties: {
                  full_name: shopDetails.shop_owner,
                  store_url: shopDetails.myshopify_domain,
                },
              },
            },
          }),
        }
      );
    } else {
      // Create new profile
      profileResponse = await fetch(`https://a.klaviyo.com/api/profiles`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Klaviyo-API-Key ${apiKey}`,
          revision: "2023-02-22",
        },
        body: JSON.stringify({
          data: {
            type: "profile",
            attributes: {
              email: shopDetails.email,
              properties: {
                full_name: shopDetails.shop_owner,
                store_url: shopDetails.myshopify_domain,
              },
            },
          },
        }),
      });
    }

    if (profileResponse.ok) {
      console.log("Successfully added or updated profile in Klaviyo");
      const profileData = await profileResponse.json();
      profileId = profileData.data.id;

      // Add profile to list
      const addToList = await fetch(
        `https://a.klaviyo.com/api/lists/${listId}/relationships/profiles/`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Klaviyo-API-Key ${apiKey}`,
            revision: "2023-02-22",
          },
          body: JSON.stringify({
            data: [
              {
                type: "profile",
                id: profileId,
              },
            ],
          }),
        }
      );

      if (addToList.ok) {
        console.log("Successfully added profile to list");
        ctx.status = 200;
        return true;
      } else {
        const errorData = await addToList.json();
        console.error("Klaviyo API error (adding to list):", errorData);
        ctx.status = addToList.status;
        ctx.body = {
          error: "Failed to add profile to Klaviyo list",
          details: errorData,
        };
        return false;
      }
    } else {
      const errorData = await profileResponse.json();
      console.error(
        "Klaviyo API error (creating/updating profile):",
        errorData
      );
      ctx.status = profileResponse.status;
      ctx.body = {
        error: "Failed to create or update profile in Klaviyo",
        details: errorData,
      };
      return false;
    }
  } catch (error) {
    console.error("Error subscribing to Klaviyo:", error);
    ctx.status = 500;
    ctx.body = { error: "Internal server error" };
    return false;
  }
}

// ****************
// controller
// ****************

// function invokes email service webhook
export async function gdprWebhook(shopDetails, appName, topic, ctx) {
  const gdprData = ctx.request.body;

  try {
    const sgMail = require("@sendgrid/mail");
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    sgMail.send({
      to: process.env.SUPPORT_EMAIL,
      from: process.env.SUPPORT_EMAIL,
      subject: `${appName} - GDPR Webhook Request - ${topic}`,
      text: `
          ${appName} GDPR Webhook content is following: \n
          App Name: ${appName} \n
          Store owner email: ${shopDetails.store_owner_email} \n
          Store Domain: ${gdprData.shop_domain} \n
          Webhook Content: ${JSON.stringify(gdprData)}
        `,
      html: `
          <p>${appName} GDPR Webhook content is following: </p>
          <p>App Name: ${appName}</p>
          <p>Store owner email: ${shopDetails.store_owner_email}</p>
          <p>Store Domain: 
            <a href="${gdprData.shop_domain}">${gdprData.shop_domain}</a>
          </p>
          <p>Webhook Content: ${JSON.stringify(gdprData)}</p>
        `,
    });
  } catch (error) {
    console.error("sendgrid error: ", error);
  }

  ctx.body = { gdprStore: { active: true } };
}

// ****************
// controller
// ****************

// function sends a Slack notification via Zapier
export async function sendSlackNotification(
  webhookUrl,
  shopDomain,
  storeOwnerEmail,
  ctx,
  notificationType
) {
  console.log("Starting sendSlackNotification...");

  console.log("shopDomain:", shopDomain);
  console.log("storeOwnerEmail:", storeOwnerEmail);

  if (!shopDomain || shopDomain.trim() === "") {
    console.error("Shop domain is missing or empty");
    ctx.throw(500, "Shop domain is missing or empty");
  }

  if (!storeOwnerEmail || storeOwnerEmail.trim() === "") {
    console.error("Store owner email is missing or empty");
    ctx.throw(500, "Store owner email is missing or empty");
  }

  if (!notificationType || notificationType.trim() === "") {
    console.error("Notification type is missing or empty");
    ctx.throw(500, "Notification type is missing or empty");
  }

  let message = "";

  if (notificationType === "new_app_install") {
    message = `<!channel> :chart_with_upwards_trend: New app installation for ${process.env.APP_NAME} - Store: <https://${shopDomain}|https://${shopDomain}> - Email: ${storeOwnerEmail}`;
  } else if (notificationType === "app_uninstall") {
    message = `<!channel> :chart_with_downwards_trend: App uninstallation for ${process.env.APP_NAME} - Store: <https://${shopDomain}|https://${shopDomain}> - Email: ${storeOwnerEmail}`;
  } else if (notificationType === "activated_subscription") {
    message = `<!channel> :money_with_wings: New subscriber for ${process.env.APP_NAME} - Store: <https://${shopDomain}|https://${shopDomain}> - Email: ${storeOwnerEmail}`;
  } else if (notificationType === "deactivated_subscription") {
    message = `<!channel> :no_entry_sign: Deactivated subscriber for ${process.env.APP_NAME} - Store: <https://${shopDomain}|https://${shopDomain}> - Email: ${storeOwnerEmail}`;
  }

  if (!message) {
    const error = "sendSlackNotification failed: no message was provided";
    console.error(error);
    ctx.throw(500, error);
  }

  // Make POST request to Zapier webhook URL
  try {
    const url = new URL(webhookUrl);
    url.searchParams.append("message", message);

    const response = await axios.post(url.toString(), null, {
      httpsAgent: new https.Agent({
        rejectUnauthorized: true,
      }),
    });

    return response.data;
  } catch (error) {
    console.error("Error sending notification to Zapier:", error);
    ctx.throw(500, "Failed to send notification to Zapier");
  }
}
