import axios from "axios";
import { db } from "../db";
// import helpers
import { getCurrentDate } from "../utils";
// import subscriptions
import { subscriptionPlans } from "../server";

// ****************
// service
// ****************

// function fetches subscription url from SHOPIFY
export const fetchSubscriptionUrl = async (
  ctx,
  access_token,
  shop,
  plan,
  price
) => {
  // ! be sure to declare correct return URL
  // TODO: remove the test parameter below for production
  const query = {
    query: `mutation {
      appSubscriptionCreate(
        name: "${plan}"
        returnUrl: "${process.env.HOST}/plan?shop=${shop}&plan=${plan}"
        test: null
        lineItems: [
          {
            plan: {
                appRecurringPricingDetails: {
                  price: { amount: ${price}, currencyCode: USD }
              }
            }
          }
        ]
      ) {
        appSubscription {
          id
          status
        }
        confirmationUrl
        userErrors {
          field
          message
        }
      }
    }`,
  };

  console.log("fetching subscription url..."); // ? debug

  try {
    const res = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      method: "POST",
      headers: { "X-Shopify-Access-Token": access_token },
      data: query,
    });
    const response = res.data;

    const subscription_data = response.data.appSubscriptionCreate;
    // console.log("subscription_data:", subscription_data); // ? debug

    return subscription_data;
  } catch (error) {
    console.error("Error fetching subscription URL:", error.message);
    if (error.response) {
      console.error("Response data:", error.response.data);
      console.error("Response status:", error.response.status);
    }
    throw new Error("Failed to fetch subscription URL");
  }
};

// ****************
// service
// ****************

// function updates subscription url in DATABASE
export const updateSubcriptionUrl = async (ctx, subscription_data) => {
  const { shop } = ctx.query;
  const { confirmationUrl, appSubscription } = subscription_data;

  const subscription_update = await db.query(
    `UPDATE shops SET
      payment_confirmation_url=$2,
      charge_id=$3,
      updated_at=$4
    WHERE
      shop_domain=$1
    RETURNING *`,
    [
      shop,
      confirmationUrl,
      appSubscription.id.split("/").slice(-1)[0],
      getCurrentDate(),
    ]
  );

  return confirmationUrl;
};

// ****************
// service
// ****************

// function cancels subscription on SHOPIFY
export const cancelSubscription_shopify = async (
  ctx,
  access_token,
  shop,
  charge_id
) => {
  const query = {
    query: `mutation {
      appSubscriptionCancel(
        id: "gid://shopify/AppSubscription/${charge_id}"
      ) {
        appSubscription {
          id
          status
        }
        userErrors {
          field
          message
        }
      }
    }`,
  };

  try {
    console.log("cancelling subscription on Shopify..."); // ? debug

    const res = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      method: "POST",
      headers: { "X-Shopify-Access-Token": access_token },
      data: query,
    });
    const subscription_cancel = res.data;

    // console.log("subscription_cancel:", subscription_cancel); // ? debug

    console.log("...subscription cancelled"); // ? debug

    return subscription_cancel;
  } catch (error) {
    console.log(
      `There was an error cancelling your subscription: ${error.response}`
    );
    ctx.throw(
      401,
      `There was an error cancelling your subscription: ${error.response}`
    );
  }
};

// ****************
// service
// ****************

// function updates db optimistically, before webhook brings in confirmed data
export async function updateSubscription_optimistic(ctx, subscription_id) {
  const subscription_status = "PENDING";

  const res = await db.query(
    `UPDATE shops SET
      subscription_plan=$2,
      subscription_status=$3,
      subscription_optimistic_update=$4
    WHERE
      shop_domain=$1
    RETURNING *`,
    [ctx.query.shop, subscription_id, subscription_status, true]
  );
  const subscription_update = res.rows[0];

  return subscription_update;
}

// ****************
// service
// ****************

// function updates subscription in DATABASE if status ACTIVE
export async function updateSubscription_db_active(
  ctx,
  subscription_name,
  subscription_status,
  webhook
) {
  const res = await db.query(
    `UPDATE shops SET
      subscription_plan=$2,
      subscription_updated_at=$3,
      last_billed_at=$4,
      updated_at=$5,
      subscription_status=$6,
      subscription_optimistic_update=$7
    WHERE
      shop_domain=$1
    RETURNING *`,
    [
      webhook.domain, // shop_domain
      subscriptionPlans.indexOf(subscription_name), // subscription_plan
      getCurrentDate(), // subscription_updated_at
      getCurrentDate(), // last_billed_at
      getCurrentDate(), // updated_at
      subscription_status, // subscription_status
      false, // subscription_optimistic_update
    ]
  );
  const subscription_update = res.rows[0];

  return subscription_update;
}

// ****************
// service
// ****************

// function updates subscription in DATABASE if status NOT active
// CANCELLED, DECLINED, EXPIRED, FROZEN, etc
export async function updateSubscription_db_cancel(
  ctx,
  subscription_status,
  webhook
) {
  console.log("reverting shop details to FREE PLAN on database..."); // ? debug

  const res = await db.query(
    `UPDATE shops SET
      subscription_status=$2,
      subscription_plan=$3,
      subscription_updated_at=$4,
      updated_at=$5,
      subscription_optimistic_update=$6,
      payment_confirmation_url=$7,
      charge_id=$8
    WHERE
      shop_domain=$1
    RETURNING *`,
    [
      webhook.domain, // shop_domain
      subscription_status, // subscription_status
      0, // subscription_plan
      getCurrentDate(), // subscription_updated_at
      getCurrentDate(), // updated_at
      false, // subscription_optimistic_update
      null, // payment_confirmation_url
      null, // charge_id
    ]
  );
  const subscription_update = res.rows[0];

  return subscription_update;
}
