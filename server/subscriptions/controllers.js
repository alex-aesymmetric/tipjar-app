import { db } from "../db";
import axios from "axios";
import { capitalize } from "lodash";
// import services
import * as services from "../services";
import * as services_meta from "../metafields/services";
import * as services_subs from "./services";
// import subscriptions
import { subscriptionPlans } from "../server";
import { sendSlackNotification } from "../controllers";

// ****************
// controller
// ****************

// function updates shop subscription
export async function updateSubcriptionUrl(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { access_token, shop_domain: shop } = shopDetails_db;
  const { plan } = ctx.request.body;

  let price;

  // if standard plan
  if (capitalize(plan) === subscriptionPlans[1]) {
    price = 4.99;
  }
  // if premium plan
  else if (capitalize(plan) === subscriptionPlans[2]) {
    price = 9.99;
  }

  const subscription_data = await services_subs.fetchSubscriptionUrl(
    ctx,
    access_token,
    shop,
    capitalize(plan),
    price
  );
  const subscription_confirmationUrl = await services_subs.updateSubcriptionUrl(
    ctx,
    subscription_data
  );

  // return subscription confirmation URL (user approves here)
  ctx.body = subscription_confirmationUrl;
}

// ****************
// controller
// ****************

// function updates the shop's subscription
// invoked from WEBHOOK
export async function updateSubscription(ctx) {
  const { webhook } = ctx.state;

  console.log("received webhook: UPDATE_SUBSCRIPTION", webhook); // ? debug

  const {
    name: subscription_name,
    status: subscription_status,
  } = webhook.payload.app_subscription;

  const shopDetails_db = await services.getShopDetails_db(ctx, webhook);
  const { store_owner_email } = shopDetails_db;

  // subscription is ACTIVE
  if (subscription_status === "ACTIVE") {
    const updateSubscription_active = await services_subs.updateSubscription_db_active(
      ctx,
      subscription_name,
      subscription_status,
      webhook
    );

    // Send Slack notification for activated subscription
    try {
      await sendSlackNotification(
        process.env.ZAPIER_SLACK_WEBHOOK_URL,
        webhook.domain,
        store_owner_email,
        ctx,
        "activated_subscription"
      );
    } catch (error) {
      console.error("Failed to send Slack notification:", error);
      // Note: We don't throw an error here to avoid disrupting the subscription update process
    }

    ctx.body = { store_data: updateSubscription_active };
  }

  // if subscription NOT active
  else if (
    subscription_status === "CANCELLED" ||
    subscription_status === "DECLINED" ||
    subscription_status === "EXPIRED" ||
    subscription_status === "FROZEN"
  ) {
    const updateSubscription_cancel = await services_subs.updateSubscription_db_cancel(
      ctx,
      subscription_status,
      webhook
    );

    // Send Slack notification for deactivated subscription
    try {
      await sendSlackNotification(
        process.env.ZAPIER_SLACK_WEBHOOK_URL,
        webhook.domain,
        store_owner_email,
        ctx,
        "deactivated_subscription"
      );
    } catch (error) {
      console.error("Failed to send Slack notification:", error);
      // Note: We don't throw an error here to avoid disrupting the subscription update process
    }

    ctx.body = { store_data: updateSubscription_cancel };
  } else {
    ctx.body = { store_data: { active: false } };
  }
}

// ****************
// controller
// ****************

// function optimistically updates shop's subscription
export async function updateSubscription_optimistic(ctx) {
  const { subscription_id } = ctx.request.body;
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { subscription_status } = shopDetails_db;

  if (
    (subscription_id === 0 && subscription_status !== "CANCELLED") ||
    (subscription_id !== 0 && subscription_status !== "ACTIVE")
  ) {
    // optimistically updates subscription status on DATABASE
    const subscription_update = await services_subs.updateSubscription_optimistic(
      ctx,
      subscription_id
    );
  }

  ctx.body = { subscription_id };
}

// ****************
// controller
// ****************

// function cancels shop's subscription
export async function cancelSubscription(ctx, webhook = null) {
  const shopDetails_db = await services.getShopDetails_db(ctx, webhook);
  const { access_token, charge_id, shop_domain: shop } = shopDetails_db;

  // if charge_id exists (only when clicking 'unsubscribe')
  if (charge_id) {
    // cancels subscription on SHOPIFY
    const cancelSubscription_shopify = await services_subs.cancelSubscription_shopify(
      ctx,
      access_token,
      shop,
      charge_id
    );
  }

  // Send Slack notification for deactivated subscription
  try {
    await sendSlackNotification(
      process.env.ZAPIER_SLACK_WEBHOOK_URL,
      shopDetails_db.shop_domain,
      ctx,
      "deactivated_subscription"
    );
  } catch (error) {
    console.error("Failed to send Slack notification:", error);
    // Note: We don't throw an error here to avoid disrupting the subscription update process
  }

  // update settings metafields on SHOPIFY and DATABASE
  const updateMetafields_cancel = await services_meta.updateMetafields_cancel(
    ctx,
    shop,
    shopDetails_db
  );

  ctx.body = "subscription cancelled";
}
