import axios from "axios";
import { db } from "../db";
// import services
import * as services from "../services";

// ****************
// service
// ****************

export async function fetchMetafields(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { access_token, shop_domain: shop } = shopDetails_db;

  try {
    const res = await axios({
      method: "GET",
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/metafields.json`,
      params: {
        namespace: "tipquik",
        key: "settings",
      },
      headers: { "X-Shopify-Access-Token": access_token },
    });

    return res.data.metafields[0];
  } catch (error) {
    ctx.throw(404, `Could not find existing shop settings: ${error}`);
  }
}

// ****************
// service
// ****************

export async function fetchMetafieldsApp(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);
  const { access_token, shop_domain: shop, metafield_app } = shopDetails_db;

  try {
    const graphqlQuery = {
      query: `query GetAppMetafields($key:String!,$namespace:String!) {
                appInstallation {
                  metafield(key: $key,namespace: $namespace) {
                    namespace
                    key
                    type
                    value
                  }
                }
              }
            `,
      variables: {
        namespace: "tipquik",
        key: "settings",
      },
    };

    const res = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      method: "POST",
      headers: { "X-Shopify-Access-Token": access_token },
      data: graphqlQuery,
    });
    return res.data.data.appInstallation.metafield;
  } catch (error) {
    ctx.throw(
      404,
      `Could not find existing app settings from Shopify: ${error}`
    );
  }
}

// ****************
// service
// ****************

export async function createMetafields_shopify(
  ctx,
  shop,
  access_token,
  metafields_value
) {
  try {
    const res = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/metafields.json`,
      method: "POST",
      headers: { "X-Shopify-Access-Token": access_token },
      data: {
        metafield: {
          namespace: "tipquik",
          key: "settings",
          value: JSON.stringify(metafields_value),
          type: "json_string",
        },
      },
    });

    const metafields_created = res.data.metafield;

    return metafields_created;
  } catch (error) {
    ctx.throw(
      400,
      `There was an error creating shop settings metafields on Shopify: ${error}`
    );
  }
}

// ****************
// service
// ****************

export async function createMetafields_db(ctx, createMetafields_shopify, shop) {
  try {
    const metafields_created = await db.query(
      `UPDATE shops SET
         metafield_id=$1
       WHERE
         shop_domain=$2`,
      [createMetafields_shopify.metafield.id, shop]
    );

    return metafields_created;
  } catch (error) {
    ctx.throw(
      500,
      `There was an error creating shop settings metafields on the database.`
    );
  }
}

// ****************
// service
// ****************

// function update shop settings metafields on SHOPIFY
export async function updateMetafields_shopify(
  ctx,
  shop,
  metafields_value,
  access_token
) {
  try {
    const res = await axios({
      method: "POST",
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/metafields.json`,
      headers: { "X-Shopify-Access-Token": access_token },
      data: {
        metafield: {
          namespace: "tipquik",
          key: "settings",
          value: JSON.stringify(metafields_value),
          type: "json_string",
          owner_resource: "shop",
        },
      },
    });

    return res.data.metafield;
  } catch (error) {
    console.error("REST API Error Details:", {
      error: error.response?.data,
      status: error.response?.status,
    });
    throw error;
  }
}

// ****************
// service
// ****************

// function update app settings metafields on SHOPIFY
export async function updateMetafields_shopifyApp(
  ctx,
  shop,
  metafield_app,
  metafields_value,
  metafields_valueOld,
  access_token
) {
  try {
    // 1. Get app installation ID - using the exact same query as Ruby
    const appInstallationQuery = `{
      currentAppInstallation {
        id
      }
    }`;

    const resGraphQLId = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      method: "POST",
      headers: {
        "X-Shopify-Access-Token": access_token,
        "Content-Type": "application/json",
      },
      data: { query: appInstallationQuery },
    });

    const appId = resGraphQLId.data?.data?.currentAppInstallation?.id;

    if (!appId) {
      throw new Error("Failed to get current app installation ID");
    }

    // 2. Prepare variables array exactly like Ruby
    const variablesArray = Object.entries(metafields_value)
      .filter(
        ([key]) => !["id", "shop_id", "created_at", "updated_at"].includes(key)
      )
      .map(([key, value]) => ({
        namespace: "tipquik",
        key: key,
        value: value === "" ? "!!BLANK!!" : value.toString(),
        type: typeof value === "boolean" ? "boolean" : "single_line_text_field",
        ownerId: appId,
      }));

    // 3. Use the exact same mutation as Ruby
    const mutation = `
      mutation CreateAppDataMetafield($metafieldsSetInput: [MetafieldsSetInput!]!) {
        metafieldsSet(metafields: $metafieldsSetInput) {
          metafields {
            id
            namespace
            key
            value
          }
          userErrors {
            field
            message
          }
        }
      }
    `;

    const response = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      method: "POST",
      headers: {
        "X-Shopify-Access-Token": access_token,
        "Content-Type": "application/json",
      },
      data: {
        query: mutation,
        variables: {
          metafieldsSetInput: variablesArray,
        },
      },
    });

    // 4. Error handling like Ruby
    const errors = response.data.errors;
    if (errors) {
      throw new Error(`GraphQL errors: ${JSON.stringify(errors)}`);
    }

    const userErrors = response.data?.data?.metafieldsSet?.userErrors;
    if (userErrors && userErrors.length > 0) {
      throw new Error(`User errors: ${JSON.stringify(userErrors)}`);
    }

    return response.data?.data?.metafieldsSet?.metafields;
  } catch (error) {
    console.error("GraphQL Error Details:", {
      error: error.response?.data?.errors,
      userErrors: error.response?.data?.data?.metafieldsSet?.userErrors,
      status: error.response?.status,
      statusText: error.response?.statusText,
    });
    throw error;
  }
}

// ****************
// service
// ****************

// function updates shop settings metafields on DATABASE
export async function updateMetafields_db(ctx, metafields_value, shop_id) {
  try {
    const res = await db.query(
      `UPDATE settings SET
        tip_percent_1=$2,
        tip_percent_2=$3,
        tip_percent_3=$4,
        enable_tip_quik=$5,
        enable_custom_tip_option=$6,
        tip_modal_title=$7,
        tip_modal_description=$8,
        tip_modal_text_color=$9,
        tip_modal_bg_color=$10,
        enable_powered_tip_quik=$11
       WHERE
        shop_id=$1
      RETURNING *`,
      [
        shop_id,
        metafields_value.tip_percent_1,
        metafields_value.tip_percent_2,
        metafields_value.tip_percent_3,
        metafields_value.enable_tip_quik,
        metafields_value.enable_custom_tip_option,
        metafields_value.tip_modal_title,
        metafields_value.tip_modal_description,
        metafields_value.tip_modal_text_color,
        metafields_value.tip_modal_bg_color,
        metafields_value.enable_powered_tip_quik,
      ]
    );
    const metafields_updated = res.rows[0];

    return metafields_updated;
  } catch (error) {
    ctx.throw(
      500,
      `There was an error updating the shop settings metafields on the database.`
    );
  }
}

// ****************
// service
// ****************

// function reverts shop settings metafields to FREE PLAN on DATABASE
export async function updateMetafields_db_cancel(ctx, shop_id) {
  // console.log("reverting settings to FREE PLAN on database..."); // ? debug

  try {
    const res = await db.query(
      `UPDATE settings SET
         enable_custom_tip_option=$1,
         enable_powered_tip_quik=$2
       WHERE
         shop_id=$3
       RETURNING *`,
      [false, true, shop_id]
    );
    const metafields_updated = res.rows[0];

    return metafields_updated;
  } catch (error) {
    ctx.throw(
      500,
      `There was an error updating the shop settings metafields on the database.`
    );
  }
}

// ****************
// service
// ****************

// function update shop settings metafields on SHOPIFY
export async function updateMetafields_cancel(ctx, shop, shopDetails_db) {
  const {
    id: shop_id,
    metafield_id,
    access_token,
    metafield_app,
  } = shopDetails_db;

  // get settings metafields from shopify
  const metafields_shopify = await fetchMetafields(ctx);
  const metafields_shopifyApp = await fetchMetafieldsApp(ctx);

  // declare metafields to update
  const metafields_update = {
    ...metafields_shopify.values,
    enable_custom_tip_option: false,
    enable_powered_tip_quik: true,
  };

  const metafields_updateApp = {
    ...metafields_shopifyApp.values,
    enable_custom_tip_option: false,
    enable_powered_tip_quik: true,
  };

  // update metafields to FREE PLAN on SHOPIFY
  const metafields_shopify_updated = await updateMetafields_shopify(
    ctx,
    shop,
    metafields_shopify.id,
    metafields_update,
    access_token,
    "CANCEL"
  );
  const metafields_shopify_updatedApp = await updateMetafields_shopifyApp(
    ctx,
    shop,
    metafield_app,
    metafields_updateApp,
    metafields_shopifyApp.values,
    access_token,
    "CANCEL"
  );

  // revert metafields to FREE PLAN on settings table in DATABASE
  const metafields_db_updated = await updateMetafields_db_cancel(ctx, shop_id);

  return metafields_shopify_updated;
}

export async function createInitialMetafields(ctx, shop, access_token) {
  try {
    // Get default settings from database
    const shopSettings_db = await services.getShopSettings_db(ctx);

    // Create both app-level and shop-level metafields
    const [appMetafields, shopMetafields] = await Promise.all([
      createMetafields_shopifyApp(ctx, shop, access_token, shopSettings_db),
      createMetafields_shopify(ctx, shop, access_token, shopSettings_db),
    ]);

    // Update database with metafield IDs
    const res = await db.query(
      `UPDATE shops SET 
        shopify_metafields_created=$2,
        metafield_id=$3,
        metafield_app=$4
      WHERE 
        shop_domain=$1 
      RETURNING *`,
      [shop, true, shopMetafields.id, appMetafields.owner_id]
    );

    return res.rows[0];
  } catch (error) {
    ctx.throw(400, `Error creating initial metafields: ${error.message}`);
  }
}

export async function createMetafields_shopifyApp(
  ctx,
  shop,
  access_token,
  settings
) {
  try {
    // Get app installation ID
    const resGraphQLId = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      method: "POST",
      headers: { "X-Shopify-Access-Token": access_token },
      data: {
        query: `{
          currentAppInstallation {
            id
          }
        }`,
      },
    });

    const owner_id = resGraphQLId.data.data.currentAppInstallation.id;

    // Create metafields array from settings
    const metafields = Object.entries(settings)
      .map(([key, value]) => {
        // Skip certain fields
        if (["id", "shop_id", "created_at", "updated_at"].includes(key)) {
          return null;
        }

        // Determine type based on value
        const type =
          typeof value === "boolean" ? "boolean" : "single_line_text_field";

        // Handle empty strings
        const metafieldValue = value === "" ? "!!BLANK!!" : value.toString();

        return {
          namespace: "tipquik",
          key,
          value: metafieldValue,
          type,
          ownerId: owner_id,
        };
      })
      .filter(Boolean);

    // Create app metafields
    const graphqlQuery = {
      query: `mutation CreateAppDataMetafield($metafieldsSetInput: [MetafieldsSetInput!]!) {
        metafieldsSet(metafields: $metafieldsSetInput) {
          metafields {
            id
            namespace
            key
          }
          userErrors {
            field
            message
          }
        }
      }`,
      variables: {
        metafieldsSetInput: metafields,
      },
    };

    const resGraphQLMetafield = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      method: "POST",
      headers: { "X-Shopify-Access-Token": access_token },
      data: graphqlQuery,
    });

    const response = resGraphQLMetafield.data.data.metafieldsSet;

    if (response.userErrors?.length) {
      throw new Error(JSON.stringify(response.userErrors));
    }

    return { metafield_app: response.metafields, owner_id };
  } catch (error) {
    throw new Error(`Failed to create app metafields: ${error.message}`);
  }
}

export async function updateMetafields(ctx) {
  try {
    const shopDetails_db = await services.getShopDetails_db(ctx);
    const { metafields_value } = ctx.request.body;
    const {
      access_token,
      metafield_id,
      metafield_app,
      shop_domain: shop,
      id: shop_id,
    } = shopDetails_db;

    // Add debug logging
    console.log("Debug - Shop Details:", {
      shop,
      hasAccessToken: !!access_token,
      accessTokenLength: access_token?.length,
      metafield_app,
      metafield_id,
    });

    if (!access_token || !shop) {
      throw new Error("Missing required shop credentials");
    }

    // Get current metafields
    const metafields_shopifyApp = await fetchMetafieldsApp(ctx);

    // Update both app and shop metafields in parallel
    const [appMetafields, shopMetafields] = await Promise.all([
      updateMetafields_shopifyApp(
        ctx,
        shop,
        metafield_app,
        metafields_value,
        metafields_shopifyApp,
        access_token
      ),
      updateMetafields_shopify(ctx, shop, metafields_value, access_token),
    ]);

    // Update database
    const dbUpdate = await updateMetafields_db(ctx, metafields_value, shop_id);

    return {
      appMetafields,
      shopMetafields,
      dbUpdate,
      success: true,
    };
  } catch (error) {
    console.error("Update Metafields Error:", error);
    if (error.response) {
      console.error("API Response:", {
        status: error.response.status,
        data: error.response.data,
        headers: error.response.headers,
      });
    }
    throw error;
  }
}
