import { db } from "../db";
// import helpers / services
import * as services from "../services";
import * as services_meta from "../metafields/services";
import * as services_subs from "../subscriptions/services";

// ****************
// controller
// ****************

// function gets existing shop settings metafields from SHOPIFY admin API
export async function fetchMetafields(ctx) {
  // fetch settings metafields from SHOPIFY
  const metafields_shopify = await services_meta.fetchMetafields(ctx);

  // console.log("metafields_shopify (controller):", metafields_shopify); // ? debug

  ctx.status = 200;
  ctx.body = metafields_shopify;
}

// ****************
// controller
// ****************

// function creates shop settings metafields if they do not exist
export async function createMetafields(ctx) {
  const shopDetails_db = await services.getShopDetails_db(ctx);

  const { access_token, shop_domain: shop } = shopDetails_db;

  // get shop settings metafields from DATABASE

  const shopSettings_db = await services.getShopSettings_db(ctx);

  // console.log(res.data);

  // create shop settings metafields on SHOPIFY
  const metafields_created_shopify = await services_meta.createMetafields_shopify(
    ctx,
    shop,
    access_token,
    shopSettings_db
  );

  // console.log(`...CREATED metafields on Shopify`); // ? debug

  const metafieldsApp = await services_meta.createMetafields_shopifyApp(
    ctx,
    shop,
    access_token,
    shopSettings_db
  );

  // update metafields_created_shopify on the SHOP table on the DATABASE
  const res = await db.query(
    `UPDATE shops SET 
        shopify_metafields_created=$2,
        metafield_id=$3,
        metafield_app=$4
      WHERE 
        shop_domain=$1 
      RETURNING *`,
    [shop, true, metafields_created_shopify.id, metafieldsApp.owner_id]
  );

  const metafields_created = res.rows[0];

  ctx.body = metafields_created;
}

// ****************
// controller
// ****************

// function updates shop settings metafields
export async function updateMetafields(ctx) {
  try {
    const result = await services_meta.updateMetafields(ctx);
    ctx.body = result;
  } catch (error) {
    console.error("Error updating metafields:", error);
    ctx.throw(400, error.message);
  }
}
