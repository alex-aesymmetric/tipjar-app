import "@babel/polyfill";
import { db } from "./db";
import dotenv from "dotenv";
import "isomorphic-fetch";
import next from "next";
const Sentry = require("@sentry/node"); // must be require
import axios from "axios";
// shopify imports
import { receiveWebhook } from "@shopify/koa-shopify-webhooks";
// koa imports
const cors = require("@koa/cors");
const bodyParser = require("koa-bodyparser");
const Koa = require("koa");
const koaConnect = require("koa-connect");
const Router = require("koa-router");
const send = require("koa-send");
// import controllers
import * as controllers from "./controllers";
import * as services from "./services";
import * as controllers_auth from "./auth/controllers";
import * as controllers_meta from "./metafields/controllers";
import * as controllers_subs from "./subscriptions/controllers";
// import middleware
import * as middleware_auth from "./auth/middleware";
const compression = require("compression");
const url = require("url");

// init and get env vars
dotenv.config();

// declare subscriptions
export const subscriptionPlans = ["Free", "Standard", "Premium"];

const port = parseInt(process.env.PORT, 10) || 8081;
const dev = process.env.NODE_ENV !== "production";

// init analytics and  app
Sentry.init({ dsn: process.env.SENTRY_DSN });
const app = next({ dev });
export const handle = app.getRequestHandler();

// ***********
// server
// ***********

// prepare next app for custom server
app.prepare().then(async () => {
  // init server and router
  const server = new Koa();
  const router = new Router();

  server.proxy = true;
  server.keys = [process.env.SHOPIFY_API_SECRET];

  // use server middleware
  server.use(koaConnect(compression()));
  server.use(cors());
  server.use(bodyParser());

  // ***********
  // routes
  // ***********

  // @route - GET * (all)
  // @desc - serve static assets without auth
  // @access - public
  router.get("/_next/*", async (ctx) => {
    await handle(ctx.req, ctx.res);
  });

  // ! currently inactive
  // @route - GET /auth
  // @desc - initialize oauth flow
  // @access - PRIVATE
  // router.get("/auth", controllers_auth.init);

  // @route - GET /auth/callback
  // @desc - finalizes oauth flow (after clicking install on oauth permission verification)
  // @access - public
  router.get("/auth/callback", controllers_auth.callback);

  // @route - GET /shop
  // @desc - get shop details from DATABASE
  // @access - PRIVATE
  router.get(
    "/shop",
    middleware_auth.verify_jwt,
    controllers.getShopDetails_db
  );

  // @route - GET /shop
  // @desc - get shop themes
  // @access - PRIVATE
  router.get("/shop/theme", middleware_auth.verify_jwt, controllers.shopThemes);

  // @route - GET /shop
  // @desc - get shop script
  // @access - PRIVATE
  router.get(
    "/shop/script-tag",
    middleware_auth.verify_jwt,
    controllers.scriptTagGet
  );

  // @route - POST /shop
  // @desc - get shop script
  // @access - PRIVATE
  router.post(
    "/shop/script-tag",
    middleware_auth.verify_jwt,
    controllers.scriptTagPost
  );
  // @route - GET /shop/settings
  // @desc - get shop settings from DATABASE
  // @access - PRIVATE
  router.get(
    "/shop/settings",
    middleware_auth.verify_jwt,
    controllers.getShopSettings_db
  );

  // ! currently inactive
  // @route - GET /shop/shopify
  // @desc - get shop settings from SHOPIFY
  // @access - PRIVATE
  router.get(
    "/shop/shopify",
    middleware_auth.verify_jwt,
    controllers.fetchShopDetails_shopify
  );

  // @route - GET /shop/metafields
  // @desc - get shop settings metafields from SHOPIFY
  // @access - PRIVATE
  router.get(
    "/shop/metafields",
    middleware_auth.verify_jwt,
    controllers_meta.fetchMetafields
  );

  // @route - POST /shop/metafields
  // @desc - creating initial shop settings metafields on SHOPIFY and DATABASE
  // @access - PRIVATE
  router.post(
    "/shop/metafields",
    middleware_auth.verify_jwt,
    controllers_meta.createMetafields
  );

  // @route - PUT /shop/metafields
  // @desc - update shop settings metafields on SHOPIFY and DATABASE
  // @access - PRIVATE
  router.put(
    "/shop/metafields",
    middleware_auth.verify_jwt,
    controllers_meta.updateMetafields
  );

  // @route - GET /script
  // @desc - return console.log back to user informing that Tipquik is available
  // @access - PUBLIC
  router.get(
    "/script",
    async (ctx) => await send(ctx, "server/scripts/tipquik.js")
  );

  // @route - POST /snippet
  // @desc - create shopify store theme snippet
  // @access - PRIVATE
  router.post(
    "/snippet",
    middleware_auth.verify_jwt,
    controllers.createSnippet
  );

  // @route - POST /product
  // @desc - create tip / gratuity product
  // @access - PRIVATE
  router.post(
    "/product",
    middleware_auth.verify_jwt,
    controllers.createProduct
  );

  // ! currently inactive
  // @route - POST /help
  // @desc - Send help request mail to support email (support@aesymmetric.xyz)
  // @access - PRIVATE
  router.post("/help", middleware_auth.verify_jwt, controllers.requestHelp);

  // ! currently inactive
  // @route - POST /plan/standard
  // @desc - join standard plan
  // @access - PRIVATE
  router.post(
    "/plan/standard",
    middleware_auth.verify_jwt,
    controllers_subs.updateSubcriptionUrl
  );

  // @route - POST /plan/premium
  // @desc - join premium plan
  // @access - PRIVATE
  router.post(
    "/plan/premium",
    middleware_auth.verify_jwt,
    controllers_subs.updateSubcriptionUrl
  );

  // @route - POST /plan/free
  // @desc - cancels shop's subscription
  // @access - PRIVATE
  router.post(
    "/plan/free",
    middleware_auth.verify_jwt,
    controllers_subs.cancelSubscription
  );

  // @route - POST /plan/optimistic
  // @desc - optimistically updates shop's subscription
  // @access - PRIVATE
  router.post(
    "/plan/optimistic",
    middleware_auth.verify_jwt,
    controllers_subs.updateSubscription_optimistic
  );

  // ! router checks shop is installed before serving app data
  // @route - GET * (all)
  // @desc - performs shop verification when app loads (and all get requests)
  // @access - PRIVATE
  router.get("*", middleware_auth.verifyShopInstall, async (ctx) => {
    // console.log("serving app page..."); // ? debug

    await handle(ctx.req, ctx.res);
  });

  // ! currently inactive
  // @route - POST
  // @desc - graphql endpoint
  // @access - PRIVATE
  router.post("/graphql", middleware_auth.verify_jwt, async (ctx, next) => {
    const shopDetails_db = await services.getShopDetails_db(ctx);
    const { shop } = ctx.query;
    const { access_token } = shopDetails_db;

    // console.log("ctx.request.body:", ctx.request.body); // ? debug

    const res = await axios({
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/graphql.json`,
      method: "POST",
      headers: { "X-Shopify-Access-Token": access_token },
      data: ctx.request.body,
    });
    const response = res.data;
    const response_gql = response.data;

    console.log("response_gql:", response_gql); // ? debug

    ctx.body = response_gql;
  });
  server.use(router.allowedMethods());
  server.use(router.routes());

  // ***********
  // web hooks
  // ***********

  // declare web hook
  const webhook = receiveWebhook({ secret: process.env.SHOPIFY_API_SECRET });

  // @webhook - POST /webhooks/uninstall
  // @desc - uninstall shop
  // @access - PUBLIC
  router.post("/webhooks/uninstall", webhook, controllers.uninstallShop);

  // @webhook - POST /webhooks/plan/update
  // @desc - update subscription
  // @access - PUBLIC
  router.post(
    "/webhooks/plan/update",
    webhook,
    controllers_subs.updateSubscription
  );

  // @route - POST /customer/request
  // @desc - webhook route
  // @access - PUBLIC
  router.post("/customer/request", async (ctx) => {
    ctx.status = 200;
    if (ctx.request && ctx.request.body) {
      const shopDetails = await controllers.getShopDetails_db(ctx);

      return controllers.gdprWebhook(
        shopDetails,
        process.env.APP_NAME,
        "customers/data_request",
        ctx
      );
    } else {
      console.log("attack");
    }
  });

  // @route - POST /customer/erase
  // @desc - webhook route
  // @access - PUBLIC
  router.post("/customer/erase", async (ctx) => {
    ctx.status = 200;
    if (ctx.request && ctx.request.body) {
      const shopDetails = await controllers.getShopDetails_db(ctx);

      return controllers.gdprWebhook(
        shopDetails,
        process.env.APP_NAME,
        "customers/redact",
        ctx
      );
    } else {
      console.log("attack");
    }
  });

  // @route - POST /shop/erase
  // @desc - webhook route
  // @access - PUBLIC
  router.post("/shop/erase", async (ctx) => {
    ctx.status = 200;
    if (ctx.request && ctx.request.body) {
      const shopDetails = await controllers.getShopDetails_db(ctx);

      return controllers.gdprWebhook(
        shopDetails,
        process.env.APP_NAME,
        "shop/redact",
        ctx
      );
    } else {
      console.log("attack");
    }
  });
  server.listen(port, () => {
    console.log(`Server ready on http://localhost:${port}`);
  });
});

export default app;
