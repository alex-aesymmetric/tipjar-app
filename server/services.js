import axios from "axios";
import { db } from "./db";
// import services / utils
import { getCurrentDate } from "./utils/getCurrentDate";

// ****************
// service
// ****************

// function gets shop details from DATABASE
export async function getShopDetails_db(ctx, webhook = null) {
  const shop = ctx.query.shop || ctx.request.body.shop_domain || webhook.domain;
  
  

  try {
    // console.log("getting shop details from database..."); // ? debug

    // console.log("shop:", shop); // ? debug

    const res = await db.query(`SELECT * FROM shops WHERE shop_domain=$1`, [
      shop,
    ]);
    const shopDetails = res.rows[0];
    

    // console.log("shopDetails:", shopDetails); // ? debug

    return shopDetails;
  } catch (error) {
    ctx.throw(404, `Couldn't find shop details in database.`);
  }
}

// ****************
// service
// ****************

// function gets shop settings from DATABASE
export async function getShopSettings_db(ctx) {
  const shopDetails_db = await getShopDetails_db(ctx);
  
  const shop_id = shopDetails_db.id;

  try {
    // console.log("getting shop settings from database..."); // ? debug

    const res = await db.query("SELECT * FROM settings WHERE shop_id=$1", [
      shop_id,
    ]);
    const shopSettings_db = res.rows[0];

    return shopSettings_db;
  } catch (error) {
    ctx.throw(404, `Couldn't find shop settings in database.`);
  }
}

// ****************
// service
// ****************

// function gets shop details from SHOPIFY admin APi
export async function fetchShopDetails_shopify(ctx, access_token_NEW = false) {
  const { shop } = ctx.query;
  let access_token;

  // check if access_token_NEW present
  // if not, fetch existing access_token
  if (access_token_NEW) {
    access_token = access_token_NEW;
  } else {
    const shopDetails_db = await getShopDetails_db(ctx);
    access_token = shopDetails_db.access_token;
  }

  try {
    if (!shop || !access_token) {
      ctx.throw(
        401,
        `Can't fetch shop info due to incorrect shop or access_token.`
      );
    }

    // console.log("fetching shop details from Shopify Admin API..."); // ? debug

    const res = await axios({
      method: "GET",
      url: `https://${shop}/admin/api/${process.env.API_VERSION}/shop.json`,
      headers: { "X-Shopify-Access-Token": access_token },
    });
    const shopDetails_shopify = res.data;
    

    // console.log("shopDetails_shopify:", shopDetails_shopify); // ? debug

    return shopDetails_shopify.shop;
  } catch (error) {
    ctx.throw(
      404,
      `Could not find existing shop settings on Shopify Admin API.`
    );
  }
}

// ****************
// service
// ****************

export async function uninstall_app_db(ctx, webhook, subscription_status) {
  // console.log("recording uninstall time on database..."); // ? debug

  const res = await db.query(
    `UPDATE shops SET 
      app_uninstalled_at=$2,
      updated_at=$3,
      subscription_status=$4
    WHERE 
      shop_domain=$1 
    RETURNING *`,
    [webhook.domain, getCurrentDate(), getCurrentDate(), subscription_status]
  );
  const uninstall_app = res.rows[0];

  return uninstall_app;
}
