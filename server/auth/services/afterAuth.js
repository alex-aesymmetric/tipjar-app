import { db } from "../../db";
import axios from "axios";
import "isomorphic-fetch"; // dep for webhooks
// import controllers / handlers / utils
import * as controllers from "../../controllers";
import * as controllers_meta from "../../metafields/controllers";
import * as handlers from "../../handlers";
import * as services from "../../services";
import * as services_meta from "../../metafields/services";
import { defaultSettings, getCurrentDate } from "../../utils";

// ****************
// service
// ****************

// function handles the registration of webhooks
export async function handleRegisterWebhooks(
  ctx,
  shopDetails_shopify,
  access_token_NEW
) {
  console.log("registering webhooks..."); // ? debug

  // uninstall webhook
  await handlers.registerWebhooks(
    "/webhooks/uninstall",
    "APP_UNINSTALLED",
    access_token_NEW,
    ctx.query.shop,
    process.env.API_VERSION
  );

  // subscription webhook
  await handlers.registerWebhooks(
    "/webhooks/plan/update",
    "APP_SUBSCRIPTIONS_UPDATE",
    access_token_NEW,
    ctx.query.shop,
    process.env.API_VERSION
  );

  // klaviyo subscription webhook
  await controllers.klaviyoSubscribe(
    process.env.KLAVIYO_API_KEY,
    process.env.KLAVIYO_LIST,
    shopDetails_shopify,
    ctx
  );

  // Send shop details to Zapier to Slack webhook
  await controllers.sendSlackNotification(
    process.env.ZAPIER_SLACK_WEBHOOK_URL,
    shopDetails_shopify.myshopify_domain,
    shopDetails_shopify.email,
    ctx,
    "new_app_install"
  );

  console.log(`...webhooks registered`); // ? debug
}

// ****************
// service
// ****************

// function resets shop and shop settings on DATABASE
export async function createShop_db(
  ctx,
  shopDetails_shopify,
  access_token_NEW
) {
  // console.log("CREATING shop on database..."); // ? debug

  try {
    // create shop details in database
    const res = await db.query(
      defaultSettings.shopInfo_INSERT_query,
      defaultSettings.shopInfo_values(
        ctx,
        access_token_NEW,
        shopDetails_shopify
      )
    );
    const insertShop = res.rows[0];
    // console.log("...CREATED shop details"); // ? debug

    const insertSettings = await db.query(
      defaultSettings.metafields_INSERT_query,
      defaultSettings.metafields_values(insertShop)
    );
    // create shop settings in database

    // console.log("...CREATED shop settings"); // ? debug

    // ! register web hooks
    await handleRegisterWebhooks(ctx, shopDetails_shopify, access_token_NEW);
  } catch (error) {
    ctx.throw(
      500,
      `There was an error CREATING the shop or shop settings on the database.`
    );
  }
}

// ****************
// service
// ****************

// function CREATES new shop and shop settings on DATABASE
export async function resetShop_db(ctx, shopDetails_shopify, access_token_NEW) {
  // console.log("RESETTING shop on database..."); // ? debug

  try {
    // create shop details in database
    const res = await db.query(
      defaultSettings.shopInfo_RESET_query,
      defaultSettings.shopInfo_values(
        ctx,
        access_token_NEW,
        shopDetails_shopify
      )
    );
    const updateShop = res.rows[0];
    // console.log("...RESET shop details"); // ? debug

    // create shop settings in database
    const updateSettings = await db.query(
      defaultSettings.metafields_RESET_query,
      defaultSettings.metafields_values(updateShop)
    );
    // console.log("...RESET shop settings"); // ? debug

    // ! register web hooks
    await handleRegisterWebhooks(ctx, shopDetails_shopify, access_token_NEW);
  } catch (error) {
    ctx.throw(
      500,
      `There was an error RESETTING the shop or shop settings on the database.`
    );
  }
}

// ****************
// service
// ****************

// function carried out after access_token granted in auth/callback
export async function afterAuth(ctx, access_token_NEW) {
  // console.log("handling after auth actions..."); // ? debug

  // console.log("ctx.query.shop (after auth):", ctx.query.shop); // ? debug

  // get shop details from SHOPIFY
  const shopDetails_shopify = await services.fetchShopDetails_shopify(
    ctx,
    access_token_NEW
  );

  // try to get shopDetails_db from DATABASE
  try {
    const shopDetails_db = await services.getShopDetails_db(ctx);

    if (shopDetails_db) {
      // console.log("...shop found on database (after auth)"); // ? debug

      // if shopDetails_db found, reset shop data on DATABASE
      await resetShop_db(ctx, shopDetails_shopify, access_token_NEW);
    } else {
      ctx.throw(404, `Shop data NOT found on database (after auth).`);
    }
  } catch (error) {
    // ! if shop details NOT found on DATABASE, create (insert) shop in DATABASE
    createShop_db(ctx, shopDetails_shopify, access_token_NEW);
  }

  // create shop settings metafields on SHOPIFY
  return controllers_meta.createMetafields(ctx);
}
