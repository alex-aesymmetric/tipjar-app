import ShopifyToken from "shopify-token";

// function generates shopify token for url auth verification
export async function createShopifyToken(ctx) {
  // console.log("generating shopify token..."); // ? debug

  const shopifyToken = new ShopifyToken({
    apiKey: process.env.SHOPIFY_API_KEY,
    redirectUri: `${process.env.HOST}/auth/callback`,
    scopes: process.env.SCOPES,
    sharedSecret: process.env.SHOPIFY_API_SECRET,
    shop: ctx.query.shop,
  });

  return shopifyToken;
}
