import { createShopifyToken } from "./createShopifyToken";

// function generates installation url
export async function createInstallUrl(ctx) {
  // console.log("generating install url..."); // ? debug

  const shopifyToken = await createShopifyToken(ctx);

  // if shop not installed on db
  try {
    // generate random nonce
    const nonce = await shopifyToken.generateNonce();
    // TODO: should store nonce in database and perform state check in auth/callback

    // generate installation url and redirect
    const url_install = await shopifyToken.generateAuthUrl(
      ctx.query.shop,
      process.env.SCOPES,
      nonce
    );
    // console.log("url_install:", url_install); // ? debug

    // redirect to install url
    ctx.redirect(url_install);
  } catch (error) {
    console.error("Error when generating redirect url for install: ", error);
    ctx.throw(400, `An error occured!  Please go back and try again.`);
  }
}
