// import controllers / helpers
import * as services from "../../services";
import * as services_auth from "../services";

export async function verifyShopInstall(ctx, next) {
  // console.log("ctx.query.shop (verify):", ctx.query.shop); // ? debug

  // if shop found in query params
  if (ctx.query.shop) {
    // ! check if shop install on DATABASE
    let shopDetails_db;
    try {
      shopDetails_db = await services.getShopDetails_db(ctx);
    } catch (error) {
      console.warn("...shop install NOT found on database (verify)"); // ? debug

      // if shop not installed on db, create and redirect to install url
      await services_auth.createInstallUrl(ctx);
    }

    // ! check if shop exists on web (SHOPIFY) to ensure it hasn't been uninstalled on web
    try {
      // console.log("access_token (verify):", shopDetails_db.access_token); // ? debug

      const shopDetails_shopify = await services.fetchShopDetails_shopify(
        ctx,
        shopDetails_db.access_token
      );
      // console.log("shopDetails_shopify:", shopDetails_shopify); // ? debug

      // if shop on SHOPIFY, serve app data
      if (shopDetails_shopify) {
        // console.log("...shop install found on Shopify (verify)"); // ? debug

        // pass onto next function (middleware)
        await next();
      }
    } catch (error) {
      console.warn("...shop install NOT found on Shopify (verify)"); // ? debug

      // create and redirect to install url
      await services_auth.createInstallUrl(ctx);
    }
  } else {
    ctx.throw(400, `Shop param missing or invalid.  Please add correct param.`);
  }
}
