const jwt = require("jsonwebtoken");

// ***********
// middleware
// ***********

export async function verify_jwt(ctx, next) {
  // console.log("ctx.path:", ctx.path); // ? debug
  // console.log("verifying token..."); // ? debug

  try {
    if (!ctx.headers.authorization) {
      ctx.throw(403, `No bearer token provided.`);
    }

    let token = ctx.headers.authorization;

    // if token starts with 'Bearer ' prefix, remove prefix from string
    if (token.startsWith("Bearer ")) {
      token = token.slice(7, token.length).trim();
    }

    if (token) {
      // console.log("token:", token); // ? debug

      // SHOPIFY_API_SECRET used as shared secret on createShopifyToken.js
      const decoded = await jwt.verify(token, process.env.SHOPIFY_API_SECRET);
      // console.log("decoded:", decoded); // ? debug

      const expiration = decoded.exp * 1000; // s -> ms
      const notBefore = decoded.nbf * 1000; // s -> ms
      const issuer = decoded.iss;
      const dest = decoded.dest;
      const audience = decoded.aud;
      const now = Date.now(); // ms
      const expired = now >= expiration;
      const tooSoon = now < notBefore;
      const topLevelMatch = issuer.includes(dest);
      const incorrectAudience = audience != process.env.SHOPIFY_API_KEY;

      // if token expired, return error
      if (expired) {
        ctx.throw(401, `token is expired.  Reauthenticate or reinstall.`);
      } else if (tooSoon) {
        ctx.throw(401, `token used too soon.  Reauthenticate or reinstall.`);
      } else if (!topLevelMatch) {
        ctx.throw(
          401,
          `top level domains don't match.  Reauthenticate or reinstall.`
        );
      } else if (incorrectAudience) {
        ctx.throw(401, `incorrect audience.  Reauthenticate or reinstall.`);
      }
    }
  } catch (error) {
    console.error(error);
    ctx.throw(401, `token is invalid.  Reauthenticate.`);
  }

  await next();
}
