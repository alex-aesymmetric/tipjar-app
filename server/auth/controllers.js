import { db } from "../db";
import axios from "axios";
// import controllers / helpers
import * as services_auth from "./services";
import * as services from "../services";

// ********
// controller
// ********

// ! currently inactive
// function invokes on /auth route
export async function init(ctx, next) {
  // console.log("ctx.query.shop (auth):", ctx.query.shop); // ? debug
  // if shop found in query params
  if (ctx.query.shop) {
    // ! check if shop install on DATABASE
    let shopDetails_db;
    try {
      shopDetails_db = await services.getShopDetails_db(ctx);
    } catch (error) {
      // console.warn("shop install NOT found in database (auth)"); // ? debug
      // if shop not installed on db, create and redirect to install url
      await createInstallUrl(ctx);
    }
    // ! check if shop exists on web (SHOPIFY) to ensure it hasn't been uninstalled on web
    try {
      // console.log("access_token (auth):", shopDetails_db.access_token); // ? debug
      const shopDetails_shopify = await services.fetchShopDetails_shopify(
        ctx,
        shopDetails_db.access_token
      );
      // console.log("shopDetails_shopify:", shopDetails_shopify); // ? debug
      // if shop on SHOPIFY, redirect
      if (shopDetails_shopify) {
        // console.log("shop install found on Shopify (auth)"); // ? debug
        // redirect to index page with shop params to serve app
        const url = `/?shop=${ctx.query.shop}`;
        ctx.redirect(url);
      }
    } catch (error) {
      console.warn("shop install NOT found on Shopify (auth)"); // ? debug
      // create and redirect to install url
      await services_auth.createInstallUrl(ctx);
    }
  } else {
    ctx.throw(400, `Shop param missing or invalid.  Please add correct param.`);
  }
}

// ********
// controller
// ********

// function invokes on auth/callback route
export async function callback(ctx) {
  const { code, shop, state, host } = ctx.query;
  const shopifyToken = await services_auth.createShopifyToken(ctx);

  //   const stateMatch = state === ctx.session.nonce;
  const stateIsString = typeof state === "string";
  const hmacVerified = await shopifyToken.verifyHmac(ctx.query);

  if (
    // TODO: removed for the sake of testing, will reimplement once oauth is verifying with other checks
    //   !stateMatch ||
    !stateIsString ||
    !hmacVerified
  ) {
    ctx.throw(400, `Security checks failed.  Please go back and try again.`);
  }

  try {
    // console.log("generating access token (auth callback)..."); // ? debug
    // generate access token

    const access_token_object = await shopifyToken.getAccessToken(shop, code);
    const access_token_NEW = access_token_object.access_token;
    // console.log("access_token_NEW (auth callback):", access_token_NEW); // ? debug
    // function handles actions after access token (after auth) issued
    await services_auth.afterAuth(ctx, access_token_NEW);
    // ! redirect to app index - end of auth
    // console.log("...end of oauth, redirecting..."); // ? debug
    // console.log("Callback-Getting host param?:", host); // ? debug
    ctx.redirect(`/?shop=${shop}&host=${host}`);
  } catch (error) {
    console.error(error);
    ctx.throw(500, `There was an error generating the access token.`);
  }
}
