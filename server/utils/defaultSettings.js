import { getCurrentDate } from "./getCurrentDate";

// ****************
// query
// ****************

export const shopInfo_INSERT_query = `
  INSERT INTO shops(
    shop_domain,
    access_token,
    store_owner_email,
    store_owner_full_name,
    subscription_status,
    subscription_plan,
    snippet_installation_status,
    product_installation_status,
    installation_help_status,
    created_at,
    updated_at,
    subscription_optimistic_update,
    shopify_metafields_created,
    metafield_app
  ) VALUES
    ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
  RETURNING *`;

export const shopInfo_RESET_query = `UPDATE shops SET
    access_token=$2,
    store_owner_email=$3,
    store_owner_full_name=$4,
    subscription_status=$5,
    subscription_plan=$6,
    snippet_installation_status=$7,
    product_installation_status=$8,
    installation_help_status=$9,
    created_at=$10,
    updated_at=$11,
    subscription_optimistic_update=$12,
    shopify_metafields_created=$13,
    metafield_app=$14
  WHERE
    shop_domain=$1
  RETURNING *`;

export const shopInfo_values = (
  ctx,
  access_token_NEW,
  shopDetails,
  subscription_status = null
) => {
  return [
    ctx.query.shop, // shop_domain
    access_token_NEW, // access_token
    shopDetails.email, // store_owner_email
    shopDetails.shop_owner, // store_owner_full_name
    subscription_status, // subscription_status
    0, // subscription_plan
    false, // snippet_installation_status
    false, // product_installation_status
    false, // installation_help_status
    getCurrentDate(), // created_at
    getCurrentDate(), // updated_at
    null, // subscription_optimistic_update
    false, // shopify_metafields_created
    null, // shopify app metafields
  ];
};

// ****************
// query
// ****************

export const metafields_INSERT_query = `INSERT INTO settings(
    shop_id,
    tip_percent_1,
    tip_percent_2,
    tip_percent_3,
    enable_tip_quik,
    enable_custom_tip_option,
    tip_modal_title,
    tip_modal_description,
    tip_modal_text_color,
    tip_modal_bg_color,
    enable_powered_tip_quik
  ) VALUES
    ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
  RETURNING *`;

export const metafields_RESET_query = `UPDATE settings SET
    tip_percent_1=$2,
    tip_percent_2=$3,
    tip_percent_3=$4,
    enable_tip_quik=$5,
    enable_custom_tip_option=$6,
    tip_modal_title=$7,
    tip_modal_description=$8,
    tip_modal_text_color=$9,
    tip_modal_bg_color=$10,
    enable_powered_tip_quik=$11
  WHERE
    shop_id=$1
  RETURNING *`;

export const metafields_values = (shop) => {
  return [
    shop.id, // shop_id
    15, // tip_percent_1
    20, // tip_percent_2
    25, // tip_percent_3
    true, // enable_tip_quik
    false, // enable_custom_tip_option
    "Do you want to leave a tip?", // tip_modal_title
    "All tips go directly to our hard working employees.", // tip_modal_description
    "#000000", // tip_modal_text_color
    "#ffffff", // tip_modal_bg_color
    true, // enable_powered_tip_quik
  ];
};
