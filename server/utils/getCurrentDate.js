import { format } from "date-fns";

export const getCurrentDate = () => {
  const date = new Date();
  const formatDate = format(date, "yyyy-MM-dd HH:mm:ss");
  return formatDate;
};
